<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="CAD Document" heading="Number"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="CAD Document" heading="File Name"
                    propertyName="CADName" type="java.lang.String">master&gt;CADName</column>
                <column alias="CAD Document" heading="Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="CAD Document" heading="State"
                    propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                <object alias="CAD Document" heading="Version" propertyName="iterationDisplayIdentifier">
                    <property name="iterationDisplayIdentifier"/>
                </object>
                <column alias="CAD Document" heading="CAD App"
                    propertyName="authoringApplication" type="wt.epm.EPMAuthoringAppType">master&gt;authoringApplication</column>
                <column alias="CAD Document" heading="Catagory"
                    propertyName="docType" type="wt.epm.EPMDocumentType">master&gt;docType</column>
                <object alias="CAD Document" heading="Created By" propertyName="creatorFullName">
                    <property name="creatorFullName"/>
                </object>
                <function heading="Created On" name="LTRIM" type="java.lang.String">
                    <column alias="CAD Document" heading="Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <object alias="CAD Document" heading="Modified By" propertyName="modifierFullName">
                    <property name="modifierFullName"/>
                </object>
                <function heading="Mofified On" name="LTRIM" type="java.lang.String">
                    <column alias="CAD Document" heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <column alias="PR" heading="PR" propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="ECR" heading="ECR" propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="CT affected" heading="CT affected"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="CT resulting" heading="CT resulting"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
            </select>
            <from>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="PR" outerJoinAlias="CAD Document">wt.change2.WTChangeIssue</table>
                <table alias="ECR" outerJoinAlias="CAD Document">wt.change2.WTChangeRequest2</table>
                <table alias="CT affected" outerJoinAlias="CAD Document">wt.change2.WTChangeActivity2</table>
                <table alias="CT resulting" outerJoinAlias="CAD Document">wt.change2.WTChangeActivity2</table>
                <table alias="CAD Document">WCTYPE|wt.epm.EPMDocument|com.ricardo.DefaultEPMDocument</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="CAD Document"
                                heading="Latest Iteration"
                                propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="1" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">1</constant>
                        </operand>
                    </condition>
                    <compositeCondition type="or">
                        <condition>
                            <operand>
                                <column alias="PR" heading="Number"
                                    propertyName="number" type="java.lang.String">master&gt;number</column>
                            </operand>
                            <nullOperator type="notNull"/>
                        </condition>
                        <condition>
                            <operand>
                                <column alias="ECR" heading="Number"
                                    propertyName="number" type="java.lang.String">master&gt;number</column>
                            </operand>
                            <nullOperator type="notNull"/>
                        </condition>
                        <condition>
                            <operand>
                                <column alias="CT affected"
                                    heading="Number"
                                    propertyName="number" type="java.lang.String">master&gt;number</column>
                            </operand>
                            <nullOperator type="notNull"/>
                        </condition>
                        <condition>
                            <operand>
                                <column alias="CT resulting"
                                    heading="Number"
                                    propertyName="number" type="java.lang.String">master&gt;number</column>
                            </operand>
                            <nullOperator type="notNull"/>
                        </condition>
                    </compositeCondition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.change2.ReportedAgainst">
                    <aliasTarget alias="PR"/>
                    <aliasTarget alias="CAD Document"/>
                </join>
                <join name="wt.change2.RelevantRequestData2">
                    <aliasTarget alias="ECR"/>
                    <aliasTarget alias="CAD Document"/>
                </join>
                <join name="wt.change2.AffectedActivityData">
                    <aliasTarget alias="CT affected"/>
                    <aliasTarget alias="CAD Document"/>
                </join>
                <join name="wt.change2.ChangeRecord2">
                    <aliasTarget alias="CT resulting"/>
                    <aliasTarget alias="CAD Document"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="CAD Document"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
