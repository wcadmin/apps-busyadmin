<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Change Task" heading="Number"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="Change Task" heading="State"
                    propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                <column alias="Change Task" heading="Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Change Task" heading="Description"
                    propertyName="description" type="java.lang.String">description</column>
                <column alias="Change Task" heading="Review Required"
                    propertyName="reviewRequired" type="boolean">reviewRequired</column>
                <function heading="Need Date" name="LTRIM" type="java.lang.String">
                    <column alias="Change Task" heading="Need Date"
                        propertyName="needDate" type="java.sql.Timestamp">needDate</column>
                </function>
                <object alias="Change Task" heading="Created By" propertyName="creatorFullName">
                    <property name="creatorFullName"/>
                </object>
                <function heading="Created On" name="LTRIM" type="java.lang.String">
                    <column alias="Change Task" heading="Created On"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <object alias="Change Task" heading="Modified By" propertyName="modifierFullName">
                    <property name="modifierFullName"/>
                </object>
                <function heading="Modified On" name="LTRIM" type="java.lang.String">
                    <column alias="Change Task" heading="Modified On"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <column alias="Change Notice" heading="ECN"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
            </select>
            <from>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="Change Notice">wt.change2.WTChangeOrder2</table>
                <table alias="Change Task">wt.change2.WTChangeActivity2</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.change2.IncludedIn2">
                    <aliasTarget alias="Change Notice"/>
                    <aliasTarget alias="Change Task"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Change Task"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
