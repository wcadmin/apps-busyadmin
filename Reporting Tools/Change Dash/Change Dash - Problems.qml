<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Problem Report" heading="Number"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="Problem Report" heading="Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Problem Report" heading="State"
                    propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                <column alias="Problem Report" heading="Description"
                    propertyName="description" type="java.lang.String">description</column>
                <column alias="Problem Report" heading="Need Date"
                    propertyName="needDate" type="java.sql.Timestamp">needDate</column>
                <column alias="Problem Report" heading="Occurrence Date"
                    propertyName="occurrenceDate" type="java.sql.Timestamp">occurrenceDate</column>
                <column alias="Problem Report" heading="Priority"
                    propertyName="issuePriority" type="wt.change2.IssuePriority">theIssuePriority</column>
                <column alias="Problem Report" heading="Requester"
                    propertyName="requester" type="java.lang.String">requester</column>
                <column alias="Problem Report" heading="Resolution Date"
                    propertyName="resolutionDate" type="java.sql.Timestamp">resolutionDate</column>
                <object alias="Problem Report" heading="Created By" propertyName="creator.fullName">
                    <property name="creator">
                        <property name="fullName"/>
                    </property>
                </object>
                <function heading="Created On" name="LTRIM" type="java.lang.String">
                    <column alias="Problem Report" heading="Created On"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <object alias="Problem Report" heading="Modified By" propertyName="modifierFullName">
                    <property name="modifierFullName"/>
                </object>
                <function heading="Modified On" name="LTRIM" type="java.lang.String">
                    <column alias="Problem Report" heading="Modified On"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <column alias="Change Request" heading="ECR"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
            </select>
            <from>
                <table alias="Problem Report">wt.change2.WTChangeIssue</table>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="Change Request" outerJoinAlias="Problem Report">wt.change2.WTChangeRequest2</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.change2.FormalizedBy">
                    <aliasTarget alias="Change Request"/>
                    <aliasTarget alias="Problem Report"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Problem Report"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
