<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Document" heading="Number"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="Document" heading="Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Document" heading="Title"
                    propertyName="title" type="java.lang.String">title</column>
                <column alias="Document" heading="State"
                    propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                <object alias="Document" heading="Version" propertyName="iterationDisplayIdentifier">
                    <property name="iterationDisplayIdentifier"/>
                </object>
                <object alias="Document" heading="Created By" propertyName="creatorFullName">
                    <property name="creatorFullName"/>
                </object>
                <function heading="Created On" name="LTRIM" type="java.lang.String">
                    <column alias="Document" heading="Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <object alias="Document" heading="Modified By" propertyName="modifierFullName">
                    <property name="modifierFullName"/>
                </object>
                <function heading="Mofified On" name="LTRIM" type="java.lang.String">
                    <column alias="Document" heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <column alias="PR" heading="PR" propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="ECR" heading="ECR" propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="CT affected" heading="CT affected"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="CT resulting" heading="CT resulting"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
            </select>
            <from>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="PR">wt.change2.WTChangeIssue</table>
                <table alias="ECR" outerJoinAlias="Document">wt.change2.WTChangeRequest2</table>
                <table alias="CT affected" outerJoinAlias="Document">wt.change2.WTChangeActivity2</table>
                <table alias="CT resulting" outerJoinAlias="Document">wt.change2.WTChangeActivity2</table>
                <table alias="Document">wt.doc.WTDocument</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                    <compositeCondition type="or">
                        <condition>
                            <operand>
                                <column alias="PR" heading="Number"
                                    propertyName="number" type="java.lang.String">master&gt;number</column>
                            </operand>
                            <nullOperator type="notNull"/>
                        </condition>
                        <condition>
                            <operand>
                                <column alias="ECR" heading="Number"
                                    propertyName="number" type="java.lang.String">master&gt;number</column>
                            </operand>
                            <nullOperator type="notNull"/>
                        </condition>
                        <condition>
                            <operand>
                                <column alias="CT affected"
                                    heading="Number"
                                    propertyName="number" type="java.lang.String">master&gt;number</column>
                            </operand>
                            <nullOperator type="notNull"/>
                        </condition>
                        <condition>
                            <operand>
                                <column alias="CT resulting"
                                    heading="Number"
                                    propertyName="number" type="java.lang.String">master&gt;number</column>
                            </operand>
                            <nullOperator type="notNull"/>
                        </condition>
                    </compositeCondition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.change2.ReportedAgainst">
                    <aliasTarget alias="PR"/>
                    <aliasTarget alias="Document"/>
                </join>
                <join name="wt.change2.RelevantRequestData2">
                    <aliasTarget alias="ECR"/>
                    <aliasTarget alias="Document"/>
                </join>
                <join name="wt.change2.AffectedActivityData">
                    <aliasTarget alias="CT affected"/>
                    <aliasTarget alias="Document"/>
                </join>
                <join name="wt.change2.ChangeRecord2">
                    <aliasTarget alias="CT resulting"/>
                    <aliasTarget alias="Document"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Document"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
