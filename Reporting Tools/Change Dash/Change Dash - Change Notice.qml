<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Change Notice" heading="Number"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="Change Notice" heading="Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Change Notice" heading="State"
                    propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                <column alias="Change Notice" heading="Description"
                    propertyName="description" type="java.lang.String">description</column>
                <column alias="Change Notice" heading="Invest Completed"
                    propertyName="investigationCompleted" type="java.lang.Boolean">investigationCompleted</column>
                <column alias="Change Notice" heading="Invest Requ"
                    propertyName="investigationRequired" type="java.lang.Boolean">investigationRequired</column>
                <function heading="Need Date" name="LTRIM" type="java.lang.String">
                    <column alias="Change Notice" heading="Need Date"
                        propertyName="needDate" type="java.sql.Timestamp">needDate</column>
                </function>
                <function heading="Resolution Date" name="LTRIM" type="java.lang.String">
                    <column alias="Change Notice"
                        heading="Resolution Date"
                        propertyName="resolutionDate" type="java.sql.Timestamp">resolutionDate</column>
                </function>
                <object alias="Change Notice" heading="Created By" propertyName="creatorFullName">
                    <property name="creatorFullName"/>
                </object>
                <function heading="Created On" name="LTRIM" type="java.lang.String">
                    <column alias="Change Notice" heading="Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <object alias="Change Notice" heading="Modified By" propertyName="modifierFullName">
                    <property name="modifierFullName"/>
                </object>
                <function heading="Modified On" name="LTRIM" type="java.lang.String">
                    <column alias="Change Notice"
                        heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <column alias="Change Request" heading="ECR"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
            </select>
            <from>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="Change Request" outerJoinAlias="Change Notice">wt.change2.WTChangeRequest2</table>
                <table alias="Change Notice">wt.change2.WTChangeOrder2</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.change2.AddressedBy2">
                    <aliasTarget alias="Change Request"/>
                    <aliasTarget alias="Change Notice"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Change Notice"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
