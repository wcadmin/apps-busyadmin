<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Change Request" heading="Number"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="Change Request" heading="Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Change Request" heading="State"
                    propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                <column alias="Change Request" heading="Bus Category"
                    propertyName="businessDecisionCategory" type="wt.change2.BusinessDecisionCategory">theBusinessDecisionCategory</column>
                <column alias="Change Request" heading="Bus Summary"
                    propertyName="businessDecisionSummary" type="java.lang.String">businessDecisionSummary</column>
                <column alias="Change Request" heading="Category"
                    propertyName="category" type="wt.change2.Category">theCategory</column>
                <column alias="Change Request" heading="Complexity"
                    propertyName="complexity" type="wt.change2.Complexity">theComplexity</column>
                <column alias="Change Request" heading="Description"
                    propertyName="description" type="java.lang.String">description</column>
                <column alias="Change Request"
                    heading="Invest Completed"
                    propertyName="investigationCompleted" type="java.lang.Boolean">investigationCompleted</column>
                <column alias="Change Request" heading="Invest Requ"
                    propertyName="investigationRequired" type="java.lang.Boolean">investigationRequired</column>
                <column alias="Change Request" heading="Need Date"
                    propertyName="needDate" type="java.sql.Timestamp">needDate</column>
                <column alias="Change Request"
                    heading="Proposed Solution"
                    propertyName="proposedSolution" type="java.lang.String">proposedSolution</column>
                <column alias="Change Request"
                    heading="Nonrecurring Cost"
                    propertyName="nonRecurringCostEst" type="java.lang.String">nonRecurringCostEst</column>
                <column alias="Change Request" heading="Recurring Cost"
                    propertyName="recurringCostEst" type="java.lang.String">recurringCostEst</column>
                <column alias="Change Request"
                    heading="Request Priority"
                    propertyName="requestPriority" type="wt.change2.RequestPriority">theRequestPriority</column>
                <column alias="Change Request" heading="Resolution Date"
                    propertyName="resolutionDate" type="java.sql.Timestamp">resolutionDate</column>
                <column alias="Change Request" heading="Review Des"
                    propertyName="reviewDescription" type="java.lang.String">reviewDescription</column>
                <object alias="Change Request" heading="Created by" propertyName="creatorFullName">
                    <property name="creatorFullName"/>
                </object>
                <function heading="Created On" name="LTRIM" type="java.lang.String">
                    <column alias="Change Request" heading="Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <object alias="Change Request" heading="Modified By" propertyName="modifierFullName">
                    <property name="modifierFullName"/>
                </object>
                <function heading="Modified On" name="LTRIM" type="java.lang.String">
                    <column alias="Change Request"
                        heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
            </select>
            <from>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="Change Request">wt.change2.WTChangeRequest2</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Change Request"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
