<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="User" heading="Username"
                    propertyName="name" type="java.lang.String">name</column>
                <column alias="User" heading="Full Name"
                    propertyName="fullName" type="java.lang.String">fullName</column>
            </select>
            <from>
                <table alias="User">wt.org.WTUser</table>
                <table alias="wt.ufid.RemoteObjectInfo">wt.ufid.RemoteObjectInfo</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="User" heading="Disabled"
                                propertyName="disabled" type="boolean">disabled</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="0" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">0</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="User" heading="Repair Needed"
                                propertyName="repairNeeded" type="boolean">repairNeeded</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="0" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">0</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="wt.ufid.RemoteObjectInfo"
                                heading="Local Object Reference.Object Id.Id"
                                propertyName="localObjectReference.objectId.id" type="long">localObjectReference.key.id</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <column alias="User"
                                heading="Persist Info.Object Identifier.Id"
                                propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="wt.ufid.RemoteObjectInfo"
                                heading="Birth Id"
                                propertyName="birthId" type="long">birthId</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="8318" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">8318</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Username"/>
                </orderByItem>
            </orderBy>
        </query>
    </statement>
</qml>
