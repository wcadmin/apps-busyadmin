<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <parameter name="User Name to Test" type="java.lang.Object"/>
    <statement>
        <query>
            <select>
                <object alias="User" heading="Full Name" propertyName="displayIdentity">
                    <property name="displayIdentity"/>
                </object>
                <object alias="User" heading="SAMA account name" propertyName="identity">
                    <property name="identity"/>
                </object>
                <function heading="Created" name="LTRIM" type="java.lang.String">
                    <column alias="User" heading="Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <function heading="Last Modified" name="LTRIM" type="java.lang.String">
                    <column alias="User" heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
            </select>
            <from>
                <table alias="User">wt.org.WTUser</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="User" heading="Disabled"
                                propertyName="disabled" type="boolean">disabled</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="1" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">1</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <function heading="Lower" name="LOWER" type="java.lang.String">
                                <parameterTarget name="User Name to Test"/>
                            </function>
                        </operand>
                        <operator type="like"/>
                        <operand>
                            <function heading="Lower" name="LOWER" type="java.lang.String">
                                <function heading="Sub String"
                                    name="SUB_STRING" type="java.lang.String">
                                    <column alias="User" heading="Name"
                                    propertyName="name" type="java.lang.String">name</column>
                                    <function heading="Add" name="ADD" type="java.math.BigDecimal">
                                    <function heading="In String"
                                    name="IN_STRING" type="java.math.BigDecimal">
                                    <column alias="User"
                                    heading="Name"
                                    propertyName="name" type="java.lang.String">name</column>
                                    <constant heading="}"
                                    isMacro="false"
                                    type="java.lang.Object" xml:space="preserve">}</constant>
                                    </function>
                                    <constant heading="1"
                                    isMacro="false"
                                    type="java.lang.Object" xml:space="preserve">1</constant>
                                    </function>
                                    <constant heading="50"
                                    isMacro="false"
                                    type="java.lang.Object" xml:space="preserve">50</constant>
                                </function>
                            </function>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
        </query>
    </statement>
</qml>
