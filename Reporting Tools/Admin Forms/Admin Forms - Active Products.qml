<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <function heading="Team Page URL" name="CONCAT" type="java.lang.String">
                    <constant
                        heading="https://windchill.ricardo.com/Windchill/app/#ptc1/product/listTeam?oid=OR%3Awt.pdmlink.PDMLinkProduct%3A"
                        type="java.lang.Object" xml:space="preserve">https://windchill.ricardo.com/Windchill/app/#ptc1/product/listTeam?oid=OR%3Awt.pdmlink.PDMLinkProduct%3A</constant>
                    <column alias="Product"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                </function>
            </select>
            <from>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
            </from>
            <where>
                <compositeCondition type="or">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Passive" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Passive</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Product"/>
                </orderByItem>
            </orderBy>
        </query>
    </statement>
</qml>
