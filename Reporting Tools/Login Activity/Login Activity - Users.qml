<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="false">
    <parameter name="Incude Enabled Users" type="java.lang.Object">
        <parameterDefault isMacro="false">
			TRUE
		</parameterDefault>
    </parameter>
    <parameter name="Incude Disabled Users" type="java.lang.Object">
        <parameterDefault isMacro="false">
			FALSE
		</parameterDefault>
    </parameter>
    <statement>
        <query>
            <select distinct="false" group="false">
                <column alias="User" heading="User ID"
                    isExternal="false"
                    propertyName="persistInfo.objectIdentifier.id"
                    selectOnly="false" type="long">
					thePersistInfo.theObjectIdentifier.id
				</column>
                <object alias="User" heading="E Mail" propertyName="EMail">
                    <property name="EMail"/>
                </object>
                <object alias="User" heading="Full Name" propertyName="fullName">
                    <property name="fullName"/>
                </object>
                <object alias="User" heading="User" propertyName=""/>
                <column alias="User" heading="Disabled"
                    isExternal="false" propertyName="disabled"
                    selectOnly="false" type="boolean">
					disabled
				</column>
                <object alias="User" heading="Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
            </select>
            <from>
                <table alias="User" isExternal="false">
					wt.org.WTUser
				</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <compositeCondition type="or">
                        <condition>
                            <operand>
                                <column alias="User" heading="Disabled"
                                    isExternal="false"
                                    propertyName="disabled"
                                    selectOnly="false" type="boolean">
									disabled
								</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="1" isMacro="false" type="java.lang.Object">
									1
								</constant>
                            </operand>
                        </condition>
                        <condition>
                            <operand>
                                <parameterTarget name="Incude Enabled Users"/>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="TRUE" isMacro="false" type="java.lang.Object">
									TRUE
								</constant>
                            </operand>
                        </condition>
                    </compositeCondition>
                    <compositeCondition type="or">
                        <condition>
                            <operand>
                                <column alias="User" heading="Disabled"
                                    isExternal="false"
                                    propertyName="disabled"
                                    selectOnly="false" type="boolean">
									disabled
								</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="0" isMacro="false" type="java.lang.Object">
									0
								</constant>
                            </operand>
                        </condition>
                        <condition>
                            <operand>
                                <parameterTarget name="Incude Disabled Users"/>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="TRUE" isMacro="false" type="java.lang.Object">
									TRUE
								</constant>
                            </operand>
                        </condition>
                    </compositeCondition>
                </compositeCondition>
            </where>
        </query>
    </statement>
</qml>
