<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="false" legacyMode="true">
    <statement>
        <query>
            <select distinct="false" group="false">
                <column alias="User" heading="User ID"
                    isExternal="false"
                    propertyName="persistInfo.objectIdentifier.id"
                    selectOnly="false" type="long">
					thePersistInfo.theObjectIdentifier.id
				</column>
                <object alias="User" heading="E Mail" propertyName="EMail">
                    <property name="EMail"/>
                </object>
                <object alias="User" heading="Full Name" propertyName="fullName">
                    <property name="fullName"/>
                </object>
                <column alias="User" heading="User Name"
                    isExternal="false" propertyName="name"
                    selectOnly="false" type="java.lang.String">
					name
				</column>
                <column alias="User" heading="Disabled"
                    isExternal="false" propertyName="disabled"
                    selectOnly="false" type="boolean">
					disabled
				</column>
                <object alias="User" heading="Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
            </select>
            <from>
                <table alias="User" isExternal="false">
					wt.org.WTUser
				</table>
            </from>
        </query>
    </statement>
</qml>
