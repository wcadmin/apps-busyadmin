<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="false">
    <statement>
        <query>
            <select distinct="false" group="false">
                <column alias="User" heading="ID" isExternal="false"
                    propertyName="persistInfo.objectIdentifier.id"
                    selectOnly="false" type="long">
					thePersistInfo.theObjectIdentifier.id
				</column>
                <object alias="User" heading="User" propertyName=""/>
                <object alias="User" heading="Name" propertyName="fullName">
                    <property name="fullName"/>
                </object>
                <object alias="User" heading="E Mail" propertyName="EMail">
                    <property name="EMail"/>
                </object>
                <object alias="User" heading="Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
            </select>
            <from>
                <table alias="User" isExternal="false">
					wt.org.WTUser
				</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="User" heading="Disabled"
                                isExternal="false"
                                propertyName="disabled"
                                selectOnly="false" type="boolean">
								disabled
							</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="0" isMacro="false" type="java.lang.Object">
								0
							</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
        </query>
    </statement>
</qml>
