<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="false">
    <parameter name="in last (days)" type="java.lang.Object">
        <parameterDefault isMacro="false">
			90
		</parameterDefault>
    </parameter>
    <statement>
        <query>
            <select distinct="false" group="false">
                <function heading="Date" name="LTRIM" type="java.lang.String">
                    <column alias="Audit Record"
                        heading="Audit Record Event Time"
                        isExternal="false" propertyName="eventTime"
                        selectOnly="false" type="java.sql.Timestamp">
							eventTime
						</column>
                </function>
                <column alias="Audit Record" heading="Container ID"
                    isExternal="false" propertyName="appContainerID.id"
                    selectOnly="false" type="long">
					appContainerID.id
				</column>
                <column alias="Audit Record" heading="User ID"
                    isExternal="false" propertyName="userID.id"
                    selectOnly="false" type="long">
					userID.id
				</column>
                <column alias="Audit Record" heading="Target Type"
                    isExternal="false" propertyName="targetType"
                    selectOnly="false" type="java.lang.String">
					targetType
				</column>
                <column alias="Audit Record" heading="Master ID"
                    isExternal="false" propertyName="masterID.id"
                    selectOnly="false" type="long">
					masterID.id
				</column>
                <column alias="Audit Record" heading="Version ID"
                    isExternal="false" propertyName="targetID.id"
                    selectOnly="false" type="long">
					targetID.id
				</column>
                <column alias="Audit Record" heading="Version"
                    isExternal="false" propertyName="versionID"
                    selectOnly="false" type="java.lang.String">
					versionID
				</column>
            </select>
            <from>
                <table alias="Audit Record" isExternal="false">
					wt.audit.AuditRecord
				</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <function heading="Time Difference(days)"
                                name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                                <function heading="System Date"
                                    name="SYSDATE" type="java.util.Date"/>
                                <column alias="Audit Record"
                                    heading="Event Time"
                                    isExternal="false"
                                    propertyName="eventTime"
                                    selectOnly="false" type="java.sql.Timestamp">
									eventTime
								</column>
                            </function>
                        </operand>
                        <operator type="lessThanOrEqual"/>
                        <operand>
                            <parameterTarget name="in last (days)"/>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Audit Record"
                                heading="Event Label" isExternal="false"
                                propertyName="eventLabel"
                                selectOnly="false" type="java.lang.String">
								eventLabel
							</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Check In" isMacro="false" type="java.lang.Object">
								Check In
							</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
        </query>
    </statement>
</qml>
