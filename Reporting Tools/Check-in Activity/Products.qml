<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="false">
    <statement>
        <query>
            <select distinct="false" group="false">
                <column alias="Product" heading="ID" isExternal="false"
                    propertyName="persistInfo.objectIdentifier.id"
                    selectOnly="false" type="long">
					thePersistInfo.theObjectIdentifier.id
				</column>
                <object alias="Product" heading="Product" propertyName=""/>
                <column alias="Product" heading="Status"
                    isExternal="false" selectOnly="false" type="java.lang.String">
					WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus
				</column>
                <function heading="Created" name="LTRIM" type="java.lang.String">
                    <column alias="Product" heading="Created"
                        isExternal="false"
                        propertyName="createTimestamp"
                        selectOnly="false" type="java.sql.Timestamp">
							thePersistInfo.createStamp
						</column>
                </function>
            </select>
            <from>
                <table alias="Product" isExternal="false">
					wt.pdmlink.PDMLinkProduct
				</table>
            </from>
            <orderBy>
                <orderByItem type="asc">
                    <columnTarget heading="Status"/>
                </orderByItem>
            </orderBy>
        </query>
    </statement>
</qml>
