<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true" legacyMode="true">
    <statement>
        <query>
            <select group="true">
                <column alias="Context" heading="Context ID"
                    propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                <column alias="Context" heading="Context Name"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="Context" heading="Context Type" propertyName="type">
                    <property name="type"/>
                </object>
                <function heading="Total context size" name="SUM" type="java.math.BigDecimal">
                    <column alias="Application Data" heading="File Size"
                        propertyName="fileSize" type="long">fileSize</column>
                </function>
                <column alias="Product" heading="Product - Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <column alias="Product" heading="Product - Project No" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|RicardoProjectNumber</column>
                <column alias="Product" heading="Product - Code Name" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|RicardoCodeName</column>
                <column alias="Product" heading="Produt - Client" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|Client</column>
                <column alias="Product" heading="Product - CAD System" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|PrimaryCadSystem</column>
            </select>
            <from>
                <table alias="Context">wt.inf.container.WTContainer</table>
                <table alias="WTContained">wt.inf.container.WTContained</table>
                <table alias="Content Holder">wt.content.ContentHolder</table>
                <table alias="Application Data">wt.content.ApplicationData</table>
                <table alias="Product" outerJoinAlias="WTContained">wt.pdmlink.PDMLinkProduct</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="WTContained"
                                heading="Persist Info.Object Identifier.Id"
                                propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <column alias="Content Holder"
                                heading="Persist Info.Object Identifier.Id"
                                propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem type="desc">
                    <columnTarget heading="Total context size"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.content.HolderToContent">
                    <aliasTarget alias="Content Holder"/>
                    <aliasTarget alias="Application Data"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="WTContained"/>
                    <aliasTarget alias="Context"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="WTContained"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
