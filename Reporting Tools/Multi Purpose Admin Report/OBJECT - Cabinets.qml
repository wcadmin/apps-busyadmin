<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="false" group="false">
                <object alias="Cabinet" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="Cabinet" heading="Personal"
                    propertyName="personalCabinet" type="boolean">personalCabinet</column>
                <column alias="Cabinet" heading="Name"
                    propertyName="name" type="java.lang.String">name</column>
                <object alias="Cabinet" heading="Domain.ID" propertyName="domainRef">
                    <property name="domainRef"/>
                </object>
                <object alias="Cabinet" heading="Container.ID" propertyName="containerReference">
                    <property name="containerReference"/>
                </object>
                <object alias="Cabinet" heading="Ownership.ID" propertyName="ownership">
                    <property name="ownership"/>
                </object>
                <object alias="Folder (wt.folder.SubFolder)"
                    heading="SubFolder.ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="Folder (wt.folder.SubFolder)"
                    heading="SubFolder.Name" propertyName="name" type="java.lang.String">name</column>
                <object alias="Folder (wt.folder.SubFolder)"
                    heading="SubFolder.Path" propertyName="folderPath">
                    <property name="folderPath"/>
                </object>
                <object alias="Folder (wt.folder.SubFolder)"
                    heading="SubFolder.Domain.ID" propertyName="domainRef">
                    <property name="domainRef"/>
                </object>
                <column alias="Folder (wt.folder.SubFolder)"
                    heading="SubFolder.Inherited Domain"
                    propertyName="inheritedDomain" type="boolean">inheritedDomain</column>
            </select>
            <from>
                <table alias="Folder (wt.folder.SubFolder)" outerJoinAlias="Cabinet">wt.folder.SubFolder</table>
                <table alias="Cabinet">wt.folder.Cabinet</table>
            </from>
            <referenceJoin>
                <join name="folderingInfo.cabinet">
                    <aliasTarget alias="Folder (wt.folder.SubFolder)"/>
                    <aliasTarget alias="Cabinet"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
