<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="type" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="type" heading="Full Name" type="java.lang.String">master&gt;intHid</column>
                <column alias="type" heading="Name" propertyName="name" type="java.lang.String">name</column>
                <column alias="type" heading="Branch Number"
                    propertyName="branchIdentifier" type="long">iterationInfo.branchId</column>
                <column alias="type" heading="User Attributeable"
                    propertyName="userAttributeable" type="boolean">userAttributeable</column>
                <column alias="type" heading="Iteration" type="java.lang.String">iterationInfo.identifier.iterationId</column>
                <column alias="type" heading="Hierarchy Display Name"
                    propertyName="hierarchyDisplayNameKey" type="java.lang.String">master&gt;hierarchyDisplayNameKey</column>
                <column alias="type" heading="Instantiable"
                    propertyName="instantiable" type="boolean">instantiable</column>
            </select>
            <from>
                <table alias="type">com.ptc.core.meta.type.mgmt.server.impl.WTTypeDefinition</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="type"
                                heading="Latest Iteration"
                                propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="1" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">1</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Full Name"/>
                </orderByItem>
            </orderBy>
        </query>
    </statement>
</qml>
