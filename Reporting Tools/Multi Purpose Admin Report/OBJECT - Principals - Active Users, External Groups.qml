<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="false">
    <statement>
        <query>
            <select distinct="false">
                <object alias="Principal" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="Principal" heading="Name"
                    propertyName="name" type="java.lang.String">name</column>
                <object alias="Principal" heading="Type" propertyName="type">
                    <property name="type"/>
                </object>
                <object alias="Principal" heading="Dn" propertyName="dn">
                    <property name="dn"/>
                </object>
                <column alias="Principal" heading="Disconnected"
                    propertyName="repairNeeded" type="boolean">repairNeeded</column>
                <column alias="Principal" heading="Internal"
                    propertyName="internal" type="boolean">internal</column>
                <object alias="Principal" heading="Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
                <function heading="Modified" name="LTRIM" type="java.lang.String">
                    <column alias="Principal" heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <function heading="Created" name="LTRIM" type="java.lang.String">
                    <column alias="Principal" heading="Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <object alias="Principal" heading="Domain.ID" propertyName="domainRef">
                    <property name="domainRef"/>
                </object>
                <object alias="Group" heading="Group.Container.ID" propertyName="container.persistInfo.objectIdentifier">
                    <property name="container">
                        <property name="persistInfo">
                            <property name="objectIdentifier"/>
                        </property>
                    </property>
                </object>
                <column alias="User" heading="User.Email"
                    propertyName="eMail" type="java.lang.String">eMail</column>
                <column alias="User" heading="User.FullName"
                    propertyName="fullName" type="java.lang.String">fullName</column>
                <object alias="User" heading="User.Org.Name" propertyName="organization.name">
                    <property name="organization">
                        <property name="name"/>
                    </property>
                </object>
            </select>
            <from>
                <table alias="Principal">wt.org.WTPrincipal</table>
                <table alias="User" outerJoinAlias="Principal">wt.org.WTUser</table>
                <table alias="Group" outerJoinAlias="Principal">wt.org.WTGroup</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Principal" heading="Disabled"
                                propertyName="disabled" type="boolean">disabled</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="0" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">0</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Principal"
                                heading="Persist Info.Object Identifier.Classname"
                                propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <column alias="User"
                                heading="Persist Info.Object Identifier.Classname"
                                propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Principal"
                                heading="Persist Info.Object Identifier.Id"
                                propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <column alias="User"
                                heading="Persist Info.Object Identifier.Id"
                                propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Principal"
                                heading="Persist Info.Object Identifier.Classname"
                                propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <column alias="Group"
                                heading="Persist Info.Object Identifier.Classname"
                                propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Principal"
                                heading="Persist Info.Object Identifier.Id"
                                propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <column alias="Group"
                                heading="Persist Info.Object Identifier.Id"
                                propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                        </operand>
                    </condition>
                    <compositeCondition type="or">
                        <compositeCondition type="and">
                            <condition>
                                <operand>
                                    <column alias="Group"
                                    heading="Internal"
                                    propertyName="internal" type="boolean">internal</column>
                                </operand>
                                <operator type="equal"/>
                                <operand>
                                    <constant heading="0"
                                    isMacro="false"
                                    type="java.lang.Object" xml:space="preserve">0</constant>
                                </operand>
                            </condition>
                            <condition>
                                <operand>
                                    <column alias="Principal"
                                    heading="Persist Info.Object Identifier.Classname"
                                    propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                                </operand>
                                <operator type="equal"/>
                                <operand>
                                    <constant heading="wt.org.WTGroup"
                                    isMacro="false"
                                    type="java.lang.Object" xml:space="preserve">wt.org.WTGroup</constant>
                                </operand>
                            </condition>
                        </compositeCondition>
                        <condition>
                            <operand>
                                <column alias="Principal"
                                    heading="Persist Info.Object Identifier.Classname"
                                    propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="wt.org.WTUser"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">wt.org.WTUser</constant>
                            </operand>
                        </condition>
                    </compositeCondition>
                </compositeCondition>
            </where>
        </query>
    </statement>
</qml>
