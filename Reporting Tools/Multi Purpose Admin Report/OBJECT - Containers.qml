<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true">
                <object alias="Context" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Context" heading="Type" propertyName="type">
                    <property name="type"/>
                </object>
                <column alias="Context" heading="Sharing"
                    propertyName="sharingEnabled" type="boolean">containerInfo.sharingEnabled</column>
                <column alias="Context" heading="Private"
                    propertyName="privateAccess" type="boolean">containerInfo.privateAccess</column>
                <column alias="Context" heading="Name"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Context" heading="Description"
                    propertyName="description" type="java.lang.String">containerInfo.description</column>
                <column alias="Product" heading="Product.Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <column alias="Product" heading="Product.Project No" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|RicardoProjectNumber</column>
                <column alias="Product" heading="Product.Codename" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|RicardoCodeName</column>
                <column alias="Product" heading="Product.Client" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|Client</column>
                <column alias="Product" heading="Product.CAD System" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|PrimaryCadSystem</column>
            </select>
            <from>
                <table alias="Context">wt.inf.container.WTContainer</table>
                <table alias="Product" outerJoinAlias="Context">wt.pdmlink.PDMLinkProduct</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Context"
                                heading="Persist Info.Object Identifier.Classname"
                                propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <column alias="Product"
                                heading="Persist Info.Object Identifier.Classname"
                                propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Context"
                                heading="Persist Info.Object Identifier.Id"
                                propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <column alias="Product"
                                heading="Persist Info.Object Identifier.Id"
                                propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
        </query>
    </statement>
</qml>
