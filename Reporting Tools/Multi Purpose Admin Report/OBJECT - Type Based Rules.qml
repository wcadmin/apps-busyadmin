<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="OIR" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="OIR" heading="Name" propertyName="name" type="java.lang.String">name</column>
                <column alias="OIR" heading="Disabled"
                    propertyName="enabledFlag" type="long">enabledFlag</column>
                <object alias="OIR" heading="Contents" propertyName="contents">
                    <property name="contents"/>
                </object>
                <function heading="Modified" name="RTRIM" type="java.lang.String">
                    <column alias="OIR" heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <function heading="Created" name="RTRIM" type="java.lang.String">
                    <column alias="OIR" heading="Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <column alias="OIR" heading="Type.Branch ID"
                    propertyName="objType" type="java.lang.String">objType</column>
                <object alias="OIR" heading="Container.ID" propertyName="containerReference">
                    <property name="containerReference"/>
                </object>
                <object alias="OIR" heading="Domain.ID" propertyName="domainRef">
                    <property name="domainRef"/>
                </object>
            </select>
            <from>
                <table alias="OIR">wt.rule.TypeBasedRule</table>
            </from>
        </query>
    </statement>
</qml>
