<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <object alias="EPMWorkspace" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="EPMWorkspace" heading="Name"
                    propertyName="name" type="java.lang.String">name</column>
                <function heading="Created Days" name="TRUNC" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="EPMWorkspace" heading="Created"
                            propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                    </function>
                    <constant heading="2" type="java.lang.Object" xml:space="preserve">2</constant>
                </function>
                <object alias="EPMWorkspace" heading="Container.ID" propertyName="containerReference">
                    <property name="containerReference"/>
                </object>
                <object alias="EPMWorkspace" heading="Principal.ID" propertyName="principalReference">
                    <property name="principalReference"/>
                </object>
                <function heading="Content.Count" name="COUNT" type="java.math.BigDecimal">
                    <column alias="Versioned"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                </function>
                <function heading="Checkpoint.Modified Days"
                    name="TRUNC" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="EPMCheckpoint"
                            heading="Last Modified"
                            propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                    </function>
                    <constant heading="2" type="java.lang.Object" xml:space="preserve">2</constant>
                </function>
            </select>
            <from>
                <table alias="EPMWorkspace">wt.epm.workspaces.EPMWorkspace</table>
                <table alias="EPMCheckpoint" outerJoinAlias="EPMWorkspace">wt.epm.workspaces.EPMCheckpoint</table>
                <table alias="Baseline Member" outerJoinAlias="EPMCheckpoint">wt.vc.baseline.BaselineMember</table>
                <table alias="Versioned" outerJoinAlias="Baseline Member">wt.vc.Versioned</table>
            </from>
            <linkJoin>
                <join name="wt.epm.workspaces.WorkspaceCheckpoint">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="EPMCheckpoint"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Baseline Member"/>
                    <aliasTarget alias="EPMCheckpoint"/>
                </join>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Baseline Member"/>
                    <aliasTarget alias="Versioned"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
