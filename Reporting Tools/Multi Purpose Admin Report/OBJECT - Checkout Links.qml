<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="Checkout Link" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Versionable" heading="Working.Item" propertyName=""/>
                <object alias="Versionable" heading="Working.Type" propertyName="type">
                    <property name="type"/>
                </object>
                <object alias="Versionable" heading="Working.ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="EPMCheckPoint"
                    heading="Working.Workspace.ID" propertyName="associatedWorkspace.persistInfo.objectIdentifier">
                    <property name="associatedWorkspace">
                        <property name="persistInfo">
                            <property name="objectIdentifier"/>
                        </property>
                    </property>
                </object>
                <function heading="Origional.Status" name="CONCAT" type="java.lang.String">
                    <column alias="CAD Document" heading="Status.Status"
                        propertyName="checkoutInfo.state" type="wt.vc.wip.WorkInProgressState">checkoutInfo.state</column>
                    <column alias="Part" heading="Part Status.Status"
                        propertyName="checkoutInfo.state" type="wt.vc.wip.WorkInProgressState">checkoutInfo.state</column>
                    <column alias="Document"
                        heading="Document - Describing (PTC) Status.Status"
                        propertyName="checkoutInfo.state" type="wt.vc.wip.WorkInProgressState">checkoutInfo.state</column>
                </function>
                <function heading="Origional.Locked.Principal.ID"
                    name="CONCAT" type="java.lang.String">
                    <constant heading="wt.org.WTUser:"
                        type="java.lang.Object" xml:space="preserve">wt.org.WTUser:</constant>
                    <column alias="Document"
                        heading="Lock.Checked Out By.Object Id.Id"
                        propertyName="lock.locker.objectId.id" type="long">lock.locker.key.id</column>
                    <column alias="Part"
                        heading="Lock.Checked Out By.Object Id.Id"
                        propertyName="lock.locker.objectId.id" type="long">lock.locker.key.id</column>
                    <column alias="CAD Document"
                        heading="Lock.Checked Out By.Object Id.Id"
                        propertyName="lock.locker.objectId.id" type="long">lock.locker.key.id</column>
                </function>
            </select>
            <from>
                <table alias="Checkout Link">wt.vc.wip.CheckoutLink</table>
                <table alias="Versionable">wt.vc.Versionable</table>
                <table alias="CAD Document" outerJoinAlias="Checkout Link">WCTYPE|wt.epm.EPMDocument|com.ricardo.DefaultEPMDocument</table>
                <table alias="Part" outerJoinAlias="Checkout Link">wt.part.WTPart</table>
                <table alias="Document" outerJoinAlias="Checkout Link">wt.doc.WTDocument</table>
                <table alias="Baseline Member" outerJoinAlias="Versionable">wt.vc.baseline.BaselineMember</table>
                <table alias="EPMCheckPoint" outerJoinAlias="Baseline Member">wt.epm.workspaces.EPMCheckpoint</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Versionable"
                                heading="Persist Info.Object Identifier.Classname"
                                propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                        </operand>
                        <inOperator type="notIn"/>
                        <inOperand>
                            <delimitedList>
                                <constant
                                    heading="wt.epm.familytable.EPMSepFamilyTable"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">wt.epm.familytable.EPMSepFamilyTable</constant>
                            </delimitedList>
                        </inOperand>
                    </condition>
                </compositeCondition>
            </where>
            <referenceJoin>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Versionable"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="CAD Document"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Part"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Document"/>
                </join>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Baseline Member"/>
                    <aliasTarget alias="Versionable"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Baseline Member"/>
                    <aliasTarget alias="EPMCheckPoint"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
