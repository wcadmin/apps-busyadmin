<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="false" group="false">
                <object alias="Membership Link" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Role" heading="Role Group.ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="Role" heading="Role Group.Name"
                    propertyName="name" type="java.lang.String">name</column>
                <object alias="Role" heading="Role Group.Container.ID" propertyName="containerReference">
                    <property name="containerReference"/>
                </object>
                <object alias="Principal" heading="Principal.ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
            </select>
            <from>
                <table alias="Parent Group" isExternal="false" outerJoinAlias="Role">
					wt.org.WTGroup
				</table>
                <table alias="Role" outerJoinAlias="Membership Link">wt.org.WTGroup</table>
                <table alias="Principal" outerJoinAlias="Membership Link">wt.org.WTPrincipal</table>
                <table alias="Membership Link">wt.org.MembershipLink</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <compositeCondition type="or">
                        <condition>
                            <operand>
                                <column alias="Parent Group"
                                    heading="Name" propertyName="name" type="java.lang.String">name</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="roleGroups"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">roleGroups</constant>
                            </operand>
                        </condition>
                        <condition>
                            <operand>
                                <column alias="Role" heading="Name"
                                    propertyName="name" type="java.lang.String">name</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="GUEST"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">GUEST</constant>
                            </operand>
                        </condition>
                    </compositeCondition>
                    <condition>
                        <operand>
                            <column alias="Role" heading="Disabled"
                                propertyName="disabled" type="boolean">disabled</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="0" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">0</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Parent Group"/>
                    <aliasTarget alias="Role"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Membership Link"/>
                    <aliasTarget alias="Role"/>
                </join>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Membership Link"/>
                    <aliasTarget alias="Principal"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
