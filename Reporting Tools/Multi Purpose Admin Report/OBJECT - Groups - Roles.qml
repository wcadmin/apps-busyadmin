<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="false">
    <statement>
        <query>
            <select distinct="false">
                <object alias="Group" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="Group" heading="Name" propertyName="name" type="java.lang.String">name</column>
                <object alias="Group" heading="Container.ID" propertyName="container.persistInfo.objectIdentifier">
                    <property name="container">
                        <property name="persistInfo">
                            <property name="objectIdentifier"/>
                        </property>
                    </property>
                </object>
            </select>
            <from>
                <table alias="Group">wt.org.WTGroup</table>
                <table alias="Parent Group" outerJoinAlias="Group">wt.org.WTGroup</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <compositeCondition type="or">
                        <condition>
                            <operand>
                                <column alias="Group" heading="Name"
                                    propertyName="name" type="java.lang.String">name</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="GUEST"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">GUEST</constant>
                            </operand>
                        </condition>
                        <condition>
                            <operand>
                                <column alias="Parent Group"
                                    heading="Name" propertyName="name" type="java.lang.String">name</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="roleGroups"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">roleGroups</constant>
                            </operand>
                        </condition>
                    </compositeCondition>
                    <condition>
                        <operand>
                            <column alias="Group" heading="Disabled"
                                propertyName="disabled" type="boolean">disabled</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="0" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">0</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Parent Group"/>
                    <aliasTarget alias="Group"/>
                </join>
            </linkJoin>
        </query>
    </statement>
</qml>
