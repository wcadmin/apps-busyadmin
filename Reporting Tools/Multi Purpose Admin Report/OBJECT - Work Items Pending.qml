<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="false">
    <statement>
        <query>
            <select distinct="false" group="false">
                <object alias="Work Item" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <function heading="Age Days" name="TRUNC" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="Work Item" heading="Created"
                            isExternal="false"
                            propertyName="createTimestamp"
                            selectOnly="false" type="java.sql.Timestamp">
							thePersistInfo.createStamp
						</column>
                    </function>
                    <constant heading="3" type="java.lang.Object" xml:space="preserve">3</constant>
                </function>
                <column alias="Work Item" heading="Role"
                    isExternal="false" propertyName="role"
                    selectOnly="false" type="wt.project.Role">
					role
				</column>
                <column alias="Work Item" heading="Reassigned"
                    isExternal="false" propertyName="reassigned"
                    selectOnly="false" type="boolean">
					reassigned
				</column>
                <column alias="Work Item" heading="Required"
                    isExternal="false" propertyName="required"
                    selectOnly="false" type="boolean">
					required
				</column>
                <column alias="Work Item" heading="Action"
                    propertyName="actionPerformed" type="java.lang.String">actionPerformed</column>
                <column alias="Work Item" heading="Status"
                    isExternal="false" propertyName="status"
                    selectOnly="false" type="wt.workflow.work.WfAssignmentState">
					status
				</column>
                <object alias="Work Item" heading="Name" propertyName=""/>
                <object alias="Work Item" heading="PBO.Number" propertyName="primaryBusinessObject.object">
                    <property name="primaryBusinessObject">
                        <property name="object"/>
                    </property>
                </object>
                <object alias="Work Item" heading="PBO.Type" propertyName="primaryBusinessObject.object.type">
                    <property name="primaryBusinessObject">
                        <property name="object">
                            <property name="type"/>
                        </property>
                    </property>
                </object>
                <function heading="PBO.Container.ID" name="CONCAT" type="java.lang.String">
                    <column alias="PR"
                        heading="Persist Info.Object Identifier.Classname"
                        propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                    <column alias="ECR"
                        heading="Persist Info.Object Identifier.Classname"
                        propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                    <column alias="ECN"
                        heading="Persist Info.Object Identifier.Classname"
                        propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                    <column alias="CT"
                        heading="Persist Info.Object Identifier.Classname"
                        propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                    <column alias="PN"
                        heading="Persist Info.Object Identifier.Classname"
                        propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                    <column alias="Var"
                        heading="Persist Info.Object Identifier.Classname"
                        propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                    <constant heading=":" type="java.lang.Object" xml:space="preserve">:</constant>
                    <column alias="PR"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                    <column alias="ECR"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                    <column alias="ECN"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                    <column alias="CT"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                    <column alias="PN"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                    <column alias="Var"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                </function>
                <object alias="Work Item" heading="Principal.ID" propertyName="ownership.owner.objectId">
                    <property name="ownership">
                        <property name="owner">
                            <property name="objectId"/>
                        </property>
                    </property>
                </object>
            </select>
            <from>
                <table alias="Work Item" isExternal="false">
					wt.workflow.work.WorkItem
				</table>
                <table alias="PR" outerJoinAlias="Work Item">wt.change2.WTChangeIssue</table>
                <table alias="ECR" outerJoinAlias="Work Item">wt.change2.WTChangeRequest2</table>
                <table alias="ECN" outerJoinAlias="Work Item">wt.change2.WTChangeOrder2</table>
                <table alias="CT" outerJoinAlias="Work Item">wt.change2.WTChangeActivity2</table>
                <table alias="PN" outerJoinAlias="Work Item">wt.maturity.PromotionNotice</table>
                <table alias="Var" outerJoinAlias="Work Item">wt.change2.WTVariance</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Work Item" heading="Status"
                                isExternal="false" propertyName="status"
                                selectOnly="false" type="wt.workflow.work.WfAssignmentState">
								status
							</column>
                        </operand>
                        <inOperator type="notIn"/>
                        <inOperand>
                            <delimitedList delimiter=",">
                                <constant heading="COMPLETED"
                                    isMacro="false" type="wt.workflow.work.WfAssignmentState">
									COMPLETED
								</constant>
                            </delimitedList>
                        </inOperand>
                    </condition>
                </compositeCondition>
            </where>
            <referenceJoin>
                <join name="primaryBusinessObject">
                    <aliasTarget alias="Work Item"/>
                    <aliasTarget alias="PR"/>
                </join>
                <join name="primaryBusinessObject">
                    <aliasTarget alias="Work Item"/>
                    <aliasTarget alias="ECR"/>
                </join>
                <join name="primaryBusinessObject">
                    <aliasTarget alias="Work Item"/>
                    <aliasTarget alias="ECN"/>
                </join>
                <join name="primaryBusinessObject">
                    <aliasTarget alias="Work Item"/>
                    <aliasTarget alias="CT"/>
                </join>
                <join name="primaryBusinessObject">
                    <aliasTarget alias="Work Item"/>
                    <aliasTarget alias="PN"/>
                </join>
                <join name="primaryBusinessObject">
                    <aliasTarget alias="Work Item"/>
                    <aliasTarget alias="Var"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
