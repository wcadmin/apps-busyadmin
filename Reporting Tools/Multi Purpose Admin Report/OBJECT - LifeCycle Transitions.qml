<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <object alias="Life Cycle" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="Life Cycle" heading="Name"
                    isExternal="false" propertyName="name"
                    selectOnly="false" type="java.lang.String">
					master&gt;name
				</column>
                <object alias="Life Cycle" heading="Cabinet.ID" propertyName="cabinetReference">
                    <property name="cabinetReference"/>
                </object>
                <object alias="Life Cycle" heading="Enabled" propertyName="enabled">
                    <property name="enabled"/>
                </object>
                <object alias="Life Cycle" heading="Routing" propertyName="routing">
                    <property name="routing"/>
                </object>
                <object alias="Life Cycle" heading="Container.ID" propertyName="container.containerReference">
                    <property name="container">
                        <property name="containerReference"/>
                    </property>
                </object>
                <column alias="Life Cycle" heading="Basic"
                    isExternal="false" propertyName="basic"
                    selectOnly="false" type="boolean">
					basic
				</column>
                <object alias="Life Cycle" heading="Master.ID" propertyName="masterReference">
                    <property name="masterReference"/>
                </object>
                <column alias="Life Cycle" heading="Master.Description"
                    isExternal="false" selectOnly="false" type="java.lang.String">
					master&gt;description
				</column>
                <column alias="Life Cycle"
                    heading="Master.SupportedClass" isExternal="false"
                    selectOnly="false" type="java.lang.String">
					master&gt;supportedClass
				</column>
                <column alias="Phase" heading="Phase.State"
                    isExternal="false" propertyName="phaseState"
                    selectOnly="false" type="wt.lifecycle.State">
					phaseState
				</column>
                <column alias="Phase" heading="Phase.Series"
                    isExternal="false" propertyName="seriesSelector"
                    selectOnly="false" type="wt.series.SeriesRangeSelector">
					seriesSelector
				</column>
                <object alias="Phase" heading="Phase.Roles" propertyName="roles">
                    <property name="roles"/>
                </object>
                <object alias="Phase" heading="Phase.Members" propertyName="members">
                    <property name="members"/>
                </object>
                <column alias="Transition"
                    heading="Phase.Transition.Name" isExternal="false"
                    propertyName="name" selectOnly="false" type="wt.lifecycle.Transition">
					name
				</column>
                <column alias="Transition Phase"
                    heading="Phase.Transition.State" isExternal="false"
                    propertyName="phaseState" selectOnly="false" type="wt.lifecycle.State">
					phaseState
				</column>
                <object alias="Phase Workflow"
                    heading="PhaseWorkflow.Name" propertyName="name">
                    <property name="name"/>
                </object>
                <object alias="Gate Workflow"
                    heading="GateWorkflow.Name" propertyName="name">
                    <property name="name"/>
                </object>
            </select>
            <from>
                <table alias="Life Cycle" isExternal="false">
					wt.lifecycle.LifeCycleTemplate
				</table>
                <table alias="Phase" isExternal="false" outerJoinAlias="Life Cycle">
					wt.lifecycle.PhaseTemplate
				</table>
                <table alias="Transition" isExternal="false" outerJoinAlias="Phase">
					wt.lifecycle.PhaseSuccession
				</table>
                <table alias="Transition Phase" isExternal="false" outerJoinAlias="Transition">
					wt.lifecycle.PhaseTemplate
				</table>
                <table alias="Ad Hoc Acl Spec" isExternal="false" outerJoinAlias="Phase">
					wt.access.AdHocAclSpec
				</table>
                <table alias="Phase Workflow" isExternal="false" outerJoinAlias="Transition Phase">
					wt.workflow.definer.WfProcessDefinition
				</table>
                <table alias="Gate Workflow" isExternal="false" outerJoinAlias="Transition Phase">
					wt.workflow.definer.WfProcessDefinition
				</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Life Cycle"
                                heading="Latest Iteration"
                                isExternal="false"
                                propertyName="latestIteration"
                                selectOnly="false" type="boolean">
								iterationInfo.latest
							</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="1" isMacro="false" type="java.lang.Object">
								1
							</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.lifecycle.PhaseLink">
                    <aliasTarget alias="Life Cycle"/>
                    <aliasTarget alias="Phase"/>
                </join>
                <join name="wt.lifecycle.AdHocAclLink">
                    <aliasTarget alias="Phase"/>
                    <aliasTarget alias="Ad Hoc Acl Spec"/>
                </join>
                <join name="wt.lifecycle.PhaseWorkflow">
                    <aliasTarget alias="Transition Phase"/>
                    <aliasTarget alias="Phase Workflow"/>
                </join>
                <join name="wt.lifecycle.GateWorkflow">
                    <aliasTarget alias="Transition Phase"/>
                    <aliasTarget alias="Gate Workflow"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Transition"/>
                    <aliasTarget alias="Phase"/>
                </join>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Transition"/>
                    <aliasTarget alias="Transition Phase"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
