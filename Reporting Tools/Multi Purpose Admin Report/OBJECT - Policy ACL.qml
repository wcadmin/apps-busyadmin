<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true">
                <object alias="Access Policy Rule" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="Access Policy Rule" heading="Type"
                    propertyName="selector.typeId" type="java.lang.String">selector.typeId</column>
                <column alias="Access Policy Rule" heading="State"
                    propertyName="selector.stateName" type="java.lang.String">selector.stateName</column>
                <object alias="Administrative Domain"
                    heading="Domain.ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <function heading="Entry.AppliesTo" name="DECODE" type="java.lang.String">
                    <column alias="WTAcl Entry"
                        heading="All Except Principal"
                        propertyName="allExceptPrincipal" type="boolean">allExceptPrincipal</column>
                    <constant heading="0" type="java.lang.Object" xml:space="preserve">0</constant>
                    <constant heading="Principal"
                        type="java.lang.Object" xml:space="preserve">Principal</constant>
                    <constant heading="All Except Principal"
                        type="java.lang.Object" xml:space="preserve">All Except Principal</constant>
                </function>
                <object alias="WTAcl Entry"
                    heading="Entry.PrincipalRef.Type" propertyName="principalReference.object.type">
                    <property name="principalReference">
                        <property name="object">
                            <property name="type"/>
                        </property>
                    </property>
                </object>
                <object alias="WTAcl Entry"
                    heading="Entry.PrincipalRef.Name" propertyName="principalReference.name">
                    <property name="principalReference">
                        <property name="name"/>
                    </property>
                </object>
                <function heading="Entry.PrincipalRef.ID" name="DECODE" type="java.lang.String">
                    <column alias="WTAcl Entry"
                        heading="Principal Reference.Object Id.Id"
                        propertyName="principalReference.objectId.id" type="long">principalReference.key.id</column>
                    <constant heading="-1" type="java.lang.Object" xml:space="preserve">-1</constant>
                    <constant heading="OWNER" type="java.lang.Object" xml:space="preserve">OWNER</constant>
                    <constant heading="-2" type="java.lang.Object" xml:space="preserve">-2</constant>
                    <constant heading="ALL" type="java.lang.Object" xml:space="preserve">ALL</constant>
                    <function heading="Concatenate" name="CONCAT" type="java.lang.String">
                        <column alias="WTAcl Entry"
                            heading="Principal Reference.Object Id.Classname"
                            propertyName="principalReference.objectId.classname" type="java.lang.String">principalReference.key.classname</column>
                        <constant heading=":" type="java.lang.Object" xml:space="preserve">:</constant>
                        <column alias="WTAcl Entry"
                            heading="Principal Reference.Object Id.Id"
                            propertyName="principalReference.objectId.id" type="long">principalReference.key.id</column>
                    </function>
                </function>
                <object alias="WTAcl Entry"
                    heading="Entry.PrincipalRef.Disabled" propertyName="principalReference.disabled">
                    <property name="principalReference">
                        <property name="disabled"/>
                    </property>
                </object>
                <object alias="WTAcl Entry"
                    heading="Entry.Permission.Type" propertyName="accessPermissionType">
                    <property name="accessPermissionType"/>
                </object>
                <object alias="WTAcl Entry" heading="Entry.Permissions" propertyName="permissions">
                    <property name="permissions"/>
                </object>
            </select>
            <from>
                <table alias="WTAcl Entry" outerJoinAlias="Access Policy Rule">wt.access.WTAclEntry</table>
                <table alias="Administrative Domain" outerJoinAlias="Access Policy Rule">wt.admin.AdministrativeDomain</table>
                <table alias="Access Policy Rule">wt.access.AccessPolicyRule</table>
            </from>
            <referenceJoin>
                <join name="domainRef">
                    <aliasTarget alias="Access Policy Rule"/>
                    <aliasTarget alias="Administrative Domain"/>
                </join>
                <join name="aclReference">
                    <aliasTarget alias="WTAcl Entry"/>
                    <aliasTarget alias="Access Policy Rule"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
