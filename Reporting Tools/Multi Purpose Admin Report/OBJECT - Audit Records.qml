<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <function heading="Event Date" name="LTRIM" type="java.lang.String">
                    <column alias="Audit Record" heading="Event Time"
                        propertyName="eventTime" type="java.sql.Timestamp">eventTime</column>
                </function>
                <function heading="Events in Day" name="COUNT" type="java.math.BigDecimal">
                    <column alias="Audit Record"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                </function>
                <column alias="Audit Record" heading="Event Label"
                    propertyName="eventLabel" type="java.lang.String">eventLabel</column>
                <column alias="Audit Record" heading="Target SubType"
                    propertyName="targetType" type="java.lang.String">targetType</column>
                <column alias="Audit Record" heading="Target Type"
                    propertyName="targetReference.objectId.classname" type="java.lang.String">targetReference.key.classname</column>
                <function heading="Container.ID" name="CONCAT" type="java.lang.String">
                    <column alias="Audit Record"
                        heading="App Container ID.Classname"
                        propertyName="appContainerID.classname" type="java.lang.String">appContainerID.classname</column>
                    <constant heading=":" type="java.lang.Object" xml:space="preserve">:</constant>
                    <column alias="Audit Record"
                        heading="App Container ID.Id"
                        propertyName="appContainerID.id" type="long">appContainerID.id</column>
                </function>
                <function heading="Principal.ID" name="CONCAT" type="java.lang.String">
                    <column alias="Audit Record"
                        heading="User ID.Classname"
                        propertyName="userID.classname" type="java.lang.String">userID.classname</column>
                    <constant heading=":" type="java.lang.Object" xml:space="preserve">:</constant>
                    <column alias="Audit Record" heading="User ID.Id"
                        propertyName="userID.id" type="long">userID.id</column>
                </function>
            </select>
            <from>
                <table alias="Audit Record" isExternal="false">
					wt.audit.AuditRecord
				</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <function heading="Time Difference(days)"
                                name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                                <function heading="System Date"
                                    name="SYSDATE" type="java.util.Date"/>
                                <column alias="Audit Record"
                                    heading="Event Time"
                                    isExternal="false"
                                    propertyName="eventTime"
                                    selectOnly="false" type="java.sql.Timestamp">
									eventTime
								</column>
                            </function>
                        </operand>
                        <operator type="lessThanOrEqual"/>
                        <operand>
                            <constant heading="365" isMacro="false"
                                type="java.math.BigDecimal" xml:space="preserve">365</constant>
                        </operand>
                    </condition>
                    <compositeCondition type="or">
                        <condition>
                            <operand>
                                <column alias="Audit Record"
                                    heading="Event Label"
                                    propertyName="eventLabel" type="java.lang.String">eventLabel</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="Check In"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">Check In</constant>
                            </operand>
                        </condition>
                        <condition>
                            <operand>
                                <column alias="Audit Record"
                                    heading="Event Label"
                                    propertyName="eventLabel" type="java.lang.String">eventLabel</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="Login"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">Login</constant>
                            </operand>
                        </condition>
                    </compositeCondition>
                </compositeCondition>
            </where>
        </query>
    </statement>
</qml>
