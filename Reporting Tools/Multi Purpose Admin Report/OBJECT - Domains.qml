<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true">
                <object alias="Domain" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Domain" heading="Container.ID" propertyName="containerReference">
                    <property name="containerReference"/>
                </object>
                <column alias="Domain" heading="Name"
                    propertyName="name" type="java.lang.String">name</column>
                <function heading="Container.TypeIndex" name="CONCAT" type="java.lang.String">
                    <function heading="Decode" name="DECODE" type="java.lang.String">
                        <column alias="Context"
                            heading="Persist Info.Object Identifier.Classname"
                            propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                        <constant
                            heading="wt.inf.container.ExchangeContainer"
                            type="java.lang.Object" xml:space="preserve">wt.inf.container.ExchangeContainer</constant>
                        <constant heading="1-Site"
                            type="java.lang.Object" xml:space="preserve">1-Site</constant>
                        <constant
                            heading="wt.inf.container.OrgContainer"
                            type="java.lang.Object" xml:space="preserve">wt.inf.container.OrgContainer</constant>
                        <constant heading="2-Org"
                            type="java.lang.Object" xml:space="preserve">2-Org</constant>
                        <constant heading="wt.pdmlink.PDMLinkProduct"
                            type="java.lang.Object" xml:space="preserve">wt.pdmlink.PDMLinkProduct</constant>
                        <constant heading="3-Prod"
                            type="java.lang.Object" xml:space="preserve">3-Prod</constant>
                        <constant heading="wt.inf.library.WTLibrary"
                            type="java.lang.Object" xml:space="preserve">wt.inf.library.WTLibrary</constant>
                        <constant heading="3-Lib"
                            type="java.lang.Object" xml:space="preserve">3-Lib</constant>
                        <constant heading="wt.projmgmt.admin.Project2"
                            type="java.lang.Object" xml:space="preserve">wt.projmgmt.admin.Project2</constant>
                        <constant heading="3-Proj"
                            type="java.lang.Object" xml:space="preserve">3-Proj</constant>
                        <constant heading="???" type="java.lang.Object" xml:space="preserve">???</constant>
                    </function>
                    <constant heading=" (" type="java.lang.Object" xml:space="preserve"> (</constant>
                    <column alias="Context" heading="Context Name"
                        propertyName="name" type="java.lang.String">containerInfo.name</column>
                    <constant heading=")" type="java.lang.Object" xml:space="preserve">)</constant>
                </function>
                <function heading="Path" name="CONCAT" type="java.lang.String">
                    <constant heading="/" type="java.lang.Object" xml:space="preserve">/</constant>
                    <column alias="p6" heading="P6.Name"
                        propertyName="name" type="java.lang.String">name</column>
                    <constant heading="/" type="java.lang.Object" xml:space="preserve">/</constant>
                    <column alias="p5" heading="P5.Name"
                        propertyName="name" type="java.lang.String">name</column>
                    <constant heading="/" type="java.lang.Object" xml:space="preserve">/</constant>
                    <column alias="p4" heading="P4.Name"
                        propertyName="name" type="java.lang.String">name</column>
                    <constant heading="/" type="java.lang.Object" xml:space="preserve">/</constant>
                    <column alias="p3" heading="P3.Name"
                        propertyName="name" type="java.lang.String">name</column>
                    <constant heading="/" type="java.lang.Object" xml:space="preserve">/</constant>
                    <column alias="p2" heading="P2.Name"
                        propertyName="name" type="java.lang.String">name</column>
                    <constant heading="/" type="java.lang.Object" xml:space="preserve">/</constant>
                    <column alias="p1" heading="P1.Name"
                        propertyName="name" type="java.lang.String">name</column>
                    <constant heading="/" type="java.lang.Object" xml:space="preserve">/</constant>
                    <column alias="Domain" heading="Name"
                        propertyName="name" type="java.lang.String">name</column>
                </function>
            </select>
            <from>
                <table alias="Domain">wt.admin.AdministrativeDomain</table>
                <table alias="p1" outerJoinAlias="Domain">wt.admin.AdministrativeDomain</table>
                <table alias="p2" outerJoinAlias="p1">wt.admin.AdministrativeDomain</table>
                <table alias="p3" outerJoinAlias="p2">wt.admin.AdministrativeDomain</table>
                <table alias="p4" outerJoinAlias="p3">wt.admin.AdministrativeDomain</table>
                <table alias="p5" outerJoinAlias="p4">wt.admin.AdministrativeDomain</table>
                <table alias="p6" outerJoinAlias="p5">wt.admin.AdministrativeDomain</table>
                <table alias="Context" outerJoinAlias="Domain">wt.inf.container.WTContainer</table>
            </from>
            <referenceJoin>
                <join name="domainRef">
                    <aliasTarget alias="Domain"/>
                    <aliasTarget alias="p1"/>
                </join>
                <join name="domainRef">
                    <aliasTarget alias="p1"/>
                    <aliasTarget alias="p2"/>
                </join>
                <join name="domainRef">
                    <aliasTarget alias="p2"/>
                    <aliasTarget alias="p3"/>
                </join>
                <join name="domainRef">
                    <aliasTarget alias="p3"/>
                    <aliasTarget alias="p4"/>
                </join>
                <join name="domainRef">
                    <aliasTarget alias="p4"/>
                    <aliasTarget alias="p5"/>
                </join>
                <join name="domainRef">
                    <aliasTarget alias="p5"/>
                    <aliasTarget alias="p6"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Domain"/>
                    <aliasTarget alias="Context"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
