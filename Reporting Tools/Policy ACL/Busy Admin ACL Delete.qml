<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <object alias="Access Policy Rule" heading="Rule - ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="WTAcl Entry"
                    heading="Principal Reference - ID" propertyName="principalReference.objectId">
                    <property name="principalReference">
                        <property name="objectId"/>
                    </property>
                </object>
                <function heading="Entry - ID (max)" name="MAX" type="java.lang.String">
                    <function heading="Concatenate" name="CONCAT" type="java.lang.String">
                        <column alias="WTAcl Entry"
                            heading="Persist Info.Object Identifier.Classname"
                            propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                        <constant heading=":" type="java.lang.Object" xml:space="preserve">:</constant>
                        <column alias="WTAcl Entry"
                            heading="Persist Info.Object Identifier.Id"
                            propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                    </function>
                </function>
                <column alias="Container"
                    heading="Rule - Container Type"
                    propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                <column alias="Container"
                    heading="Rule - Container Name" propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Parent Org Container"
                    heading="Rule - Parent Organization Name"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="Access Policy Rule"
                    heading="Rule - Domain Name" propertyName="domainRef.name">
                    <property name="domainRef">
                        <property name="name"/>
                    </property>
                </object>
                <column alias="Access Policy Rule" heading="Rule - Type"
                    propertyName="selector.typeId" type="java.lang.String">selector.typeId</column>
                <column alias="Access Policy Rule"
                    heading="Rule - State"
                    propertyName="selector.stateName" type="java.lang.String">selector.stateName</column>
                <column alias="WTAcl Entry"
                    heading="Entry - All Except Principal"
                    propertyName="allExceptPrincipal" type="boolean">allExceptPrincipal</column>
                <column alias="WTAcl Entry"
                    heading="Entry - Principal Type"
                    propertyName="principalReference.objectId.classname" type="java.lang.String">principalReference.key.classname</column>
                <object alias="WTAcl Entry"
                    heading="Entry - Principal Name" propertyName="principalReference.name">
                    <property name="principalReference">
                        <property name="name"/>
                    </property>
                </object>
            </select>
            <from>
                <table alias="WTAcl Entry">wt.access.WTAclEntry</table>
                <table alias="Access Policy Rule">wt.access.AccessPolicyRule</table>
                <table alias="Container">wt.inf.container.WTContainer</table>
                <table alias="Parent Org Container" outerJoinAlias="Container">wt.inf.container.OrgContainer</table>
                <table alias="Administrative Domain">wt.admin.AdministrativeDomain</table>
            </from>
            <linkJoin>
                <join name="wt.access.AclEntryLink">
                    <aliasTarget alias="Access Policy Rule"/>
                    <aliasTarget alias="WTAcl Entry"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Container"/>
                    <aliasTarget alias="Parent Org Container"/>
                </join>
                <join name="domainRef">
                    <aliasTarget alias="Access Policy Rule"/>
                    <aliasTarget alias="Administrative Domain"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Administrative Domain"/>
                    <aliasTarget alias="Container"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
