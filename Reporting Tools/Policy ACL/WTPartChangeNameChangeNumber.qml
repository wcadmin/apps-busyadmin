<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="Part"
                    heading="Persist Info.Object Identifier.Id" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="Part" heading="Number"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="Part" heading="Name" propertyName="name" type="java.lang.String">master&gt;name</column>
                <object alias="Part" heading="Context (containerName)" propertyName="containerName">
                    <property name="containerName"/>
                </object>
                <object alias="Part" heading="Lock.Checked Out By.Name" propertyName="lock.locker.name">
                    <property name="lock">
                        <property name="locker">
                            <property name="name"/>
                        </property>
                    </property>
                </object>
            </select>
            <from>
                <table alias="Part">wt.part.WTPart</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Part"
                                heading="versionInfo.identifier.versionSortId" type="java.lang.String">versionInfo.identifier.versionSortId</column>
                        </operand>
                        <inOperator type="in"/>
                        <inOperand>
                            <subQuery>
                                <subQuerySelect>
                                    <function heading="Maximum"
                                    name="MAX" type="java.lang.String">
                                    <column alias="Part"
                                    heading="versionInfo.identifier.versionSortId" type="java.lang.String">versionInfo.identifier.versionSortId</column>
                                    </function>
                                </subQuerySelect>
                                <from>
                                    <table alias="Part 1">wt.part.WTPart</table>
                                </from>
                                <where>
                                    <compositeCondition type="and">
                                    <condition>
                                    <operand>
                                    <column alias="Part"
                                    heading="master>thePersistInfo.theObjectIdentifier.id" type="long">master&gt;thePersistInfo.theObjectIdentifier.id</column>
                                    </operand>
                                    <operator type="equal"/>
                                    <operand>
                                    <column alias="Part 1"
                                    heading="master>thePersistInfo.theObjectIdentifier.id" type="long">master&gt;thePersistInfo.theObjectIdentifier.id</column>
                                    </operand>
                                    </condition>
                                    </compositeCondition>
                                </where>
                            </subQuery>
                        </inOperand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Part"
                                heading="Latest Iteration"
                                propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="1" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">1</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
        </query>
    </statement>
</qml>
