<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="Access Policy Rule"
                    heading="AccessPolicyRuleRef" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="WTAcl Entry" heading="WTAclEntryRef" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="Administrative Domain" heading="Domain"
                    propertyName="name" type="java.lang.String">name</column>
                <object alias="Administrative Domain" heading="Context" propertyName="container">
                    <property name="container"/>
                </object>
                <column alias="Access Policy Rule" heading="Type"
                    propertyName="selector.typeId" type="java.lang.String">selector.typeId</column>
                <column alias="Access Policy Rule" heading="State"
                    propertyName="selector.stateName" type="java.lang.String">selector.stateName</column>
                <object alias="WTAcl Entry" heading="Principal" propertyName="principalReference.name">
                    <property name="principalReference">
                        <property name="name"/>
                    </property>
                </object>
                <function heading="Applies To" name="DECODE" type="java.lang.String">
                    <function heading="Truncate" name="TRUNC" type="java.math.BigDecimal">
                        <column alias="WTAcl Entry"
                            heading="All Except Principal"
                            propertyName="allExceptPrincipal" type="boolean">allExceptPrincipal</column>
                    </function>
                    <constant heading="1" type="java.lang.Object" xml:space="preserve">1</constant>
                    <constant heading="All Except Principal"
                        type="java.lang.Object" xml:space="preserve">All Except Principal</constant>
                    <constant heading="0" type="java.lang.Object" xml:space="preserve">0</constant>
                    <constant heading="Principal 1"
                        type="java.lang.Object" xml:space="preserve">Principal</constant>
                    <constant heading="screw you"
                        type="java.lang.Object" xml:space="preserve">screw you</constant>
                </function>
                <object alias="WTAcl Entry" heading="Permissions" propertyName="permissions">
                    <property name="permissions"/>
                </object>
            </select>
            <from>
                <table alias="Access Policy Rule">wt.access.AccessPolicyRule</table>
                <table alias="WTAcl Entry">wt.access.WTAclEntry</table>
                <table alias="Administrative Domain">wt.admin.AdministrativeDomain</table>
            </from>
            <referenceJoin>
                <join name="aclReference">
                    <aliasTarget alias="WTAcl Entry"/>
                    <aliasTarget alias="Access Policy Rule"/>
                </join>
                <join name="selector.ownerRef">
                    <aliasTarget alias="Access Policy Rule"/>
                    <aliasTarget alias="Administrative Domain"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
