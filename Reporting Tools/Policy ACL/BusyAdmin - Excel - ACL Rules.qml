<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="Access Policy Rule" heading="Rule - ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <function heading="Rule - Name Hash" name="CONCAT" type="java.lang.String">
                    <column alias="Container"
                        heading="Persist Info.Object Identifier.Classname"
                        propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                    <constant heading=" # " type="java.lang.Object" xml:space="preserve"> # </constant>
                    <column alias="Container" heading="Context Name"
                        propertyName="name" type="java.lang.String">containerInfo.name</column>
                    <constant heading=" # " type="java.lang.Object" xml:space="preserve"> # </constant>
                    <column alias="Domain" heading="Name"
                        propertyName="name" type="java.lang.String">name</column>
                    <constant heading=" # " type="java.lang.Object" xml:space="preserve"> # </constant>
                    <column alias="Access Policy Rule"
                        heading="Selector.Type Id"
                        propertyName="selector.typeId" type="java.lang.String">selector.typeId</column>
                    <constant heading=" # " type="java.lang.Object" xml:space="preserve"> # </constant>
                    <column alias="Access Policy Rule"
                        heading="Selector.State Name"
                        propertyName="selector.stateName" type="java.lang.String">selector.stateName</column>
                </function>
                <column alias="Container"
                    heading="Rule - Container Type"
                    propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                <column alias="Container"
                    heading="Rule - Container Name" propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Parent Org Container"
                    heading="Rule - Parent Organization Name"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="Access Policy Rule"
                    heading="Rule - Domain Name" propertyName="domainRef.name">
                    <property name="domainRef">
                        <property name="name"/>
                    </property>
                </object>
                <column alias="Access Policy Rule" heading="Rule - Type"
                    propertyName="selector.typeId" type="java.lang.String">selector.typeId</column>
                <column alias="Access Policy Rule"
                    heading="Rule - State"
                    propertyName="selector.stateName" type="java.lang.String">selector.stateName</column>
                <object alias="WTAcl Entry" heading="Entry - ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <function heading="Entry - Name Hash" name="CONCAT" type="java.lang.String">
                    <column alias="WTAcl Entry"
                        heading="All Except Principal"
                        propertyName="allExceptPrincipal" type="boolean">allExceptPrincipal</column>
                    <constant heading=" # " type="java.lang.Object" xml:space="preserve"> # </constant>
                    <column alias="WTAcl Entry"
                        heading="Principal Reference.Object Id.Classname"
                        propertyName="principalReference.objectId.classname" type="java.lang.String">principalReference.key.classname</column>
                    <constant heading=" # " type="java.lang.Object" xml:space="preserve"> # </constant>
                    <column alias="Principal" heading="Name"
                        propertyName="name" type="java.lang.String">name</column>
                    <constant heading=" # " type="java.lang.Object" xml:space="preserve"> # </constant>
                    <column alias="WTAcl Entry" heading="permissionType" type="int">permissionType</column>
                    <constant heading=" # " type="java.lang.Object" xml:space="preserve"> # </constant>
                    <column alias="WTAcl Entry" heading="permissionMask" type="long">permissionMask</column>
                </function>
                <column alias="WTAcl Entry"
                    heading="Entry - All Except Principal"
                    propertyName="allExceptPrincipal" type="boolean">allExceptPrincipal</column>
                <column alias="WTAcl Entry"
                    heading="Entry - Principal Type"
                    propertyName="principalReference.objectId.classname" type="java.lang.String">principalReference.key.classname</column>
                <object alias="WTAcl Entry"
                    heading="Entry - Principal Name" propertyName="principalReference.name">
                    <property name="principalReference">
                        <property name="name"/>
                    </property>
                </object>
                <object alias="WTAcl Entry"
                    heading="Entry - Permission Type" propertyName="accessPermissionType">
                    <property name="accessPermissionType"/>
                </object>
                <column alias="WTAcl Entry"
                    heading="Entry - Permission Mask" type="long">permissionMask</column>
                <object alias="WTAcl Entry"
                    heading="Entry - Permission Set" propertyName="permissionSet">
                    <property name="permissionSet"/>
                </object>
            </select>
            <from>
                <table alias="WTAcl Entry">wt.access.WTAclEntry</table>
                <table alias="Access Policy Rule">wt.access.AccessPolicyRule</table>
                <table alias="Domain" outerJoinAlias="Access Policy Rule">wt.admin.AdministrativeDomain</table>
                <table alias="Container" outerJoinAlias="Domain">wt.inf.container.WTContainer</table>
                <table alias="Parent Org Container" outerJoinAlias="Container">wt.inf.container.OrgContainer</table>
                <table alias="Principal" outerJoinAlias="WTAcl Entry">wt.org.WTPrincipal</table>
            </from>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Rule - Name Hash"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Entry - Name Hash"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.access.AclEntryLink">
                    <aliasTarget alias="Access Policy Rule"/>
                    <aliasTarget alias="WTAcl Entry"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="principalReference">
                    <aliasTarget alias="WTAcl Entry"/>
                    <aliasTarget alias="Principal"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Container"/>
                    <aliasTarget alias="Parent Org Container"/>
                </join>
                <join name="domainRef">
                    <aliasTarget alias="Access Policy Rule"/>
                    <aliasTarget alias="Domain"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Domain"/>
                    <aliasTarget alias="Container"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
