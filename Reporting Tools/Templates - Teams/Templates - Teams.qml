<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Team (wt.team.TeamTemplate)"
                    heading="Name" propertyName="name" type="java.lang.String">name</column>
                <column alias="Team (wt.team.TeamTemplate)"
                    heading="Enabled" propertyName="enabled" type="boolean">enabled</column>
                <object alias="Team (wt.team.TeamTemplate)"
                    heading="Members" propertyName="members">
                    <property name="members"/>
                </object>
                <object alias="Team (wt.team.TeamTemplate)"
                    heading="Role Actor Role Map" propertyName="roleActorRoleMap">
                    <property name="roleActorRoleMap"/>
                </object>
                <object alias="Team (wt.team.TeamTemplate)"
                    heading="Role Principal Map" propertyName="rolePrincipalMap">
                    <property name="rolePrincipalMap"/>
                </object>
                <object alias="Team (wt.team.TeamTemplate)"
                    heading="Roles" propertyName="roles">
                    <property name="roles"/>
                </object>
                <column alias="Team (wt.team.TeamTemplate)"
                    heading="Container ID"
                    propertyName="containerReference.objectId.id" type="long">containerReference.key.id</column>
                <column alias="Team (wt.team.TeamTemplate)"
                    heading="Container Type"
                    propertyName="containerReference.objectId.classname" type="java.lang.String">containerReference.key.classname</column>
                <object alias="Team (wt.team.TeamTemplate)"
                    heading="Container Name" propertyName="containerName">
                    <property name="containerName"/>
                </object>
                <function heading="Created" name="LTRIM" type="java.lang.String">
                    <column alias="Team (wt.team.TeamTemplate)"
                        heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <function heading="Modified" name="LTRIM" type="java.lang.String">
                    <column alias="Team (wt.team.TeamTemplate)"
                        heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
            </select>
            <from>
                <table alias="Team (wt.team.TeamTemplate)">wt.team.TeamTemplate</table>
            </from>
        </query>
    </statement>
</qml>
