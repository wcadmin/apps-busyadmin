<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <object alias="instance" heading="Instance Folder" propertyName="location">
                    <property name="location"/>
                </object>
                <column alias="instance master" heading="Instance Name"
                    propertyName="name" type="java.lang.String">name</column>
                <column alias="instance master"
                    heading="Instance Number" propertyName="number" type="java.lang.String">number</column>
                <column alias="instance" heading="Instance State"
                    propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                <column alias="generic" heading="Generic Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="generic" heading="Generic CAD App"
                    propertyName="authoringApplication" type="wt.epm.EPMAuthoringAppType">master&gt;authoringApplication</column>
                <object alias="generic" heading="Generic Folder" propertyName="location">
                    <property name="location"/>
                </object>
                <column alias="assembly context" heading="Assy Context"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="assembly" heading="Assy Number"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <column alias="assembly" heading="Assy Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <function heading="Usage Count" name="COUNT" type="java.math.BigDecimal">
                    <column alias="uses link"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                </function>
            </select>
            <from>
                <table alias="library">wt.inf.library.WTLibrary</table>
                <table alias="instance master">WCTYPE|wt.epm.EPMDocumentMaster|com.ricardo.DefaultEPMDocumentMaster</table>
                <table alias="assembly" outerJoinAlias="uses link">WCTYPE|wt.epm.EPMDocument|com.ricardo.DefaultEPMDocument</table>
                <table alias="uses link" outerJoinAlias="instance master">wt.epm.structure.EPMMemberLink</table>
                <table alias="assembly context" outerJoinAlias="assembly">wt.inf.container.WTContainer</table>
                <table alias="instance">WCTYPE|wt.epm.EPMDocument|com.ricardo.DefaultEPMDocument</table>
                <table alias="generic">wt.epm.familytable.EPMSepFamilyTable</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="library"
                                heading="Context Name"
                                propertyName="name" type="java.lang.String">containerInfo.name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Ricardo Parts Library"
                                isMacro="false" type="java.lang.String" xml:space="preserve">Ricardo Parts Library</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="instance"
                                heading="Latest Iteration"
                                propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="1" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">1</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="instance"
                                heading="Ownership.Owner.Object Id.Classname"
                                propertyName="ownership.owner.objectId.classname" type="java.lang.String">ownership.owner.key.classname</column>
                        </operand>
                        <nullOperator type="isNull"/>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="generic"
                                heading="Ownership.Owner.Object Id.Classname"
                                propertyName="ownership.owner.objectId.classname" type="java.lang.String">ownership.owner.key.classname</column>
                        </operand>
                        <nullOperator type="isNull"/>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Instance Name"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.epm.structure.EPMContainedIn">
                    <aliasTarget alias="instance"/>
                    <aliasTarget alias="generic"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="roleAObjectRef">
                    <aliasTarget alias="uses link"/>
                    <aliasTarget alias="assembly"/>
                </join>
                <join name="roleBObjectRef">
                    <aliasTarget alias="uses link"/>
                    <aliasTarget alias="instance master"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="instance master"/>
                    <aliasTarget alias="library"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="assembly"/>
                    <aliasTarget alias="assembly context"/>
                </join>
                <join name="masterReference">
                    <aliasTarget alias="instance"/>
                    <aliasTarget alias="instance master"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
