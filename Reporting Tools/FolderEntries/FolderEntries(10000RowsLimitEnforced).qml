<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="Folder Entry"
                    heading="paramFolderEntryRef" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Folder Entry" heading="Folder Path" propertyName="folderPath">
                    <property name="folderPath"/>
                </object>
                <object alias="Folder Entry" heading="Identity" propertyName="identity">
                    <property name="identity"/>
                </object>
                <object alias="Folder Entry" heading="Cabinet" propertyName="cabinetReference">
                    <property name="cabinetReference"/>
                </object>
                <object alias="Folder Entry" heading="Location" propertyName="location">
                    <property name="location"/>
                </object>
                <object alias="Folder Entry" heading="Name" propertyName="name">
                    <property name="name"/>
                </object>
                <object alias="Folder Entry" heading="Persist Info" propertyName="persistInfo">
                    <property name="persistInfo"/>
                </object>
                <object alias="Folder Entry" heading="Type" propertyName="type">
                    <property name="type"/>
                </object>
            </select>
            <from>
                <table alias="Folder Entry">wt.folder.FolderEntry</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <keyword heading="Row Number" name="rownum" type="java.math.BigDecimal"/>
                        </operand>
                        <operator type="lessThan"/>
                        <operand>
                            <constant heading="10000" isMacro="false"
                                type="java.math.BigDecimal" xml:space="preserve">10000</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
        </query>
    </statement>
</qml>
