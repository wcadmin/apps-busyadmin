<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="false">
    <statement>
        <query>
            <select distinct="false">
                <column alias="Product" heading="Product Client" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|Client</column>
                <column alias="Product" heading="Product Name"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Product" heading="Product Project Number" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|RicardoProjectNumber</column>
                <column alias="Product" heading="Product Code Name" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|RicardoCodeName</column>
                <column alias="Product" heading="Product CAD" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|PrimaryCadSystem</column>
                <function heading="Product Created" name="LTRIM" type="java.lang.String">
                    <column alias="Product" heading="Product Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <column alias="Group" heading="Role Name"
                    propertyName="name" type="java.lang.String">name</column>
                <object alias="User" heading="User Repo" propertyName="repository">
                    <property name="repository"/>
                </object>
                <column alias="User" heading="User Email"
                    propertyName="eMail" type="java.lang.String">eMail</column>
                <column alias="User" heading="User Full Name"
                    propertyName="fullName" type="java.lang.String">fullName</column>
                <column alias="User" heading="User Name"
                    propertyName="name" type="java.lang.String">name</column>
                <function heading="User Created" name="LTRIM" type="java.lang.String">
                    <column alias="User" heading="User Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
            </select>
            <from>
                <table alias="Group">wt.org.WTGroup</table>
                <table alias="Parent Group" outerJoinAlias="Group">wt.org.WTGroup</table>
                <table alias="User">wt.org.WTUser</table>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <compositeCondition type="or">
                        <condition>
                            <operand>
                                <column alias="Group" heading="Name"
                                    propertyName="name" type="java.lang.String">name</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="GUEST"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">GUEST</constant>
                            </operand>
                        </condition>
                        <condition>
                            <operand>
                                <column alias="Parent Group"
                                    heading="Name" propertyName="name" type="java.lang.String">name</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="roleGroups"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">roleGroups</constant>
                            </operand>
                        </condition>
                    </compositeCondition>
                    <condition>
                        <operand>
                            <column alias="Group" heading="Disabled"
                                propertyName="disabled" type="boolean">disabled</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="0" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">0</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="User" heading="Disabled"
                                propertyName="disabled" type="boolean">disabled</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="0" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">0</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Group"
                                heading="Repair Needed"
                                propertyName="repairNeeded" type="boolean">repairNeeded</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="0" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">0</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Parent Group"/>
                    <aliasTarget alias="Group"/>
                </join>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Group"/>
                    <aliasTarget alias="User"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Group"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
