<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <function heading="Days Checked Out" name="FLOOR" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="Checkout Link" heading="Created"
                            propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                    </function>
                </function>
                <object alias="Versionable" heading="Type" propertyName="type">
                    <property name="type"/>
                </object>
                <object alias="Versionable" heading="Checked Out Item" propertyName=""/>
                <object alias="Checkout Link" heading="Email" propertyName="workingCopy.ownership.owner.EMail">
                    <property name="workingCopy">
                        <property name="ownership">
                            <property name="owner">
                                <property name="EMail"/>
                            </property>
                        </property>
                    </property>
                </object>
                <object alias="Checkout Link" heading="User" propertyName="workingCopy.ownership.owner.name">
                    <property name="workingCopy">
                        <property name="ownership">
                            <property name="owner">
                                <property name="name"/>
                            </property>
                        </property>
                    </property>
                </object>
                <object alias="EPMWorkspace" heading="Workspace Name" propertyName=""/>
                <object alias="EPMWorkspace" heading="Workspace Context" propertyName="container">
                    <property name="container"/>
                </object>
            </select>
            <from>
                <table alias="Checkout Link">wt.vc.wip.CheckoutLink</table>
                <table alias="Versionable">wt.vc.Versionable</table>
                <table alias="Baseline Member" outerJoinAlias="Versionable">wt.vc.baseline.BaselineMember</table>
                <table alias="wt.epm.workspaces.EPMCheckpoint" outerJoinAlias="Baseline Member">wt.epm.workspaces.EPMCheckpoint</table>
                <table alias="EPMWorkspace" outerJoinAlias="wt.epm.workspaces.EPMCheckpoint">wt.epm.workspaces.EPMWorkspace</table>
            </from>
            <referenceJoin>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Versionable"/>
                </join>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Baseline Member"/>
                    <aliasTarget alias="Versionable"/>
                </join>
                <join name="checkpointReference">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="wt.epm.workspaces.EPMCheckpoint"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Baseline Member"/>
                    <aliasTarget alias="wt.epm.workspaces.EPMCheckpoint"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
