<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <object alias="EPM Workspace" heading="Context Type" propertyName="container.type">
                    <property name="container">
                        <property name="type"/>
                    </property>
                </object>
                <object alias="EPM Workspace" heading="Context Name" propertyName="container.name">
                    <property name="container">
                        <property name="name"/>
                    </property>
                </object>
                <column alias="Product" heading="Product Status"
                    isExternal="false" selectOnly="false" type="java.lang.String">
					WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus
				</column>
                <column alias="Product" heading="Product CAD system"
                    isExternal="false" selectOnly="false" type="java.lang.String">
					WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|PrimaryCadSystem
				</column>
                <column alias="User" heading="WS Owner User"
                    isExternal="false" propertyName="name"
                    selectOnly="false" type="java.lang.String">
					name
				</column>
                <object alias="User" heading="WS Owner Email" propertyName="EMail">
                    <property name="EMail"/>
                </object>
                <object alias="User" heading="WS Owner Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
                <object alias="EPM Workspace" heading="WS Name" propertyName=""/>
                <function heading="WS Item Count" name="COUNT" type="java.math.BigDecimal">
                    <column alias="Baseline Member"
                        heading="roleBObjectRef.key.id"
                        isExternal="false" selectOnly="false" type="long">
						roleBObjectRef.key.id
					</column>
                </function>
                <function heading="WS Days Since Created" name="FLOOR" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="EPM Workspace" heading="Created"
                            isExternal="false"
                            propertyName="createTimestamp"
                            selectOnly="false" type="java.sql.Timestamp">
							thePersistInfo.createStamp
						</column>
                    </function>
                </function>
                <function heading="WS Days Since Used" name="FLOOR" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="EPM checkpoint"
                            heading="Last Modified" isExternal="false"
                            propertyName="modifyTimestamp"
                            selectOnly="false" type="java.sql.Timestamp">
							thePersistInfo.modifyStamp
						</column>
                    </function>
                </function>
            </select>
            <from>
                <table alias="Baseline Member" isExternal="false" outerJoinAlias="EPM checkpoint">
					wt.vc.baseline.BaselineMember
				</table>
                <table alias="EPM checkpoint" isExternal="false" outerJoinAlias="EPM Workspace">
					wt.epm.workspaces.EPMCheckpoint
				</table>
                <table alias="EPM Workspace" isExternal="false">
					wt.epm.workspaces.EPMWorkspace
				</table>
                <table alias="Product" isExternal="false" outerJoinAlias="EPM Workspace">
					wt.pdmlink.PDMLinkProduct
				</table>
                <table alias="User" isExternal="false" outerJoinAlias="EPM Workspace">
					wt.org.WTUser
				</table>
            </from>
            <linkJoin>
                <join name="wt.epm.workspaces.WorkspaceCheckpoint">
                    <aliasTarget alias="EPM Workspace"/>
                    <aliasTarget alias="EPM checkpoint"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Baseline Member"/>
                    <aliasTarget alias="EPM checkpoint"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="EPM Workspace"/>
                    <aliasTarget alias="Product"/>
                </join>
                <join name="principalReference">
                    <aliasTarget alias="EPM Workspace"/>
                    <aliasTarget alias="User"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
