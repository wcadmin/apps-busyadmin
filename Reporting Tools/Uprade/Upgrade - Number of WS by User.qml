<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <column alias="User" heading="WS Owner User"
                    isExternal="false" propertyName="name"
                    selectOnly="false" type="java.lang.String">
					name
				</column>
                <object alias="User" heading="WS Owner Email" propertyName="EMail">
                    <property name="EMail"/>
                </object>
                <function heading="WC Count" name="COUNT" type="java.math.BigDecimal">
                    <column alias="EPM Workspace"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                </function>
            </select>
            <from>
                <table alias="EPM Workspace" isExternal="false">
					wt.epm.workspaces.EPMWorkspace
				</table>
                <table alias="User" isExternal="false" outerJoinAlias="EPM Workspace">
					wt.org.WTUser
				</table>
            </from>
            <orderBy>
                <orderByItem type="desc">
                    <columnTarget heading="WC Count"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="WS Owner User"/>
                </orderByItem>
            </orderBy>
            <referenceJoin>
                <join name="principalReference">
                    <aliasTarget alias="EPM Workspace"/>
                    <aliasTarget alias="User"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
