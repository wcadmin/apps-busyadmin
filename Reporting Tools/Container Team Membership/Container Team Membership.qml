<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <object alias="Context" heading="Container Type" propertyName="type">
                    <property name="type"/>
                </object>
                <column alias="Context" heading="Container Name"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Role" heading="Container Team Role"
                    propertyName="name" type="java.lang.String">name</column>
                <column alias="Role Member level 1"
                    heading="Member - Name" propertyName="name" type="java.lang.String">name</column>
                <column alias="Role Member level 1"
                    heading="Member - Disabled" propertyName="disabled" type="boolean">disabled</column>
                <column alias="Role Member level 1"
                    heading="Member - Repair Needed"
                    propertyName="repairNeeded" type="boolean">repairNeeded</column>
                <object alias="Role Member level 1"
                    heading="Member - Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
                <object alias="Role Member level 1"
                    heading="Member - Principal Type" propertyName="type">
                    <property name="type"/>
                </object>
                <column alias="Product" heading="Product -  Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <column alias="Product"
                    heading="Product - Project Number" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|RicardoProjectNumber</column>
                <column alias="Product" heading="Product - Code Name" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|RicardoCodeName</column>
                <column alias="Product" heading="Product - Client" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|Client</column>
                <column alias="Product" heading="Product - CAD System" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|PrimaryCadSystem</column>
                <column alias="Project (wt.projmgmt.admin.Project2)"
                    heading="Project - Active" propertyName="activeFlag" type="boolean">containerTeamManagedInfo.activeFlag</column>
                <column alias="Project (wt.projmgmt.admin.Project2)"
                    heading="Project - Completion Status"
                    propertyName="completionStatus" type="int">completionStatus</column>
                <column alias="Project (wt.projmgmt.admin.Project2)"
                    heading="Project - Ctm State"
                    propertyName="ctmState" type="wt.inf.team.ContainerTeamManagedState">containerTeamManagedInfo.state</column>
                <column alias="Project (wt.projmgmt.admin.Project2)"
                    heading="Project - Phase" propertyName="phase" type="wt.projmgmt.admin.ProjectPhase">phase</column>
                <column alias="Project (wt.projmgmt.admin.Project2)"
                    heading="Project - Private Access"
                    propertyName="privateAccess" type="boolean">containerInfo.privateAccess</column>
                <column alias="Project (wt.projmgmt.admin.Project2)"
                    heading="Project - Pseudo Type"
                    propertyName="pseudoType" type="int">pseudoType</column>
                <column alias="Project (wt.projmgmt.admin.Project2)"
                    heading="Project - Status"
                    propertyName="healthStatus" type="wt.projmgmt.admin.ProjectHealthStatus">healthStatus</column>
                <object alias="Context" heading="ID Container" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Role Member level 1"
                    heading="ID Principal" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
            </select>
            <from>
                <table alias="Role" isExternal="false">
					wt.org.WTGroup
				</table>
                <table alias="Role Member level 1" isExternal="false" outerJoinAlias="Role">
					wt.org.WTPrincipal
				</table>
                <table alias="Context">wt.inf.container.WTContainer</table>
                <table alias="Role Parent Group" outerJoinAlias="Role">wt.org.WTGroup</table>
                <table alias="Product" outerJoinAlias="Role">wt.pdmlink.PDMLinkProduct</table>
                <table alias="Project (wt.projmgmt.admin.Project2)" outerJoinAlias="Role">wt.projmgmt.admin.Project2</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <compositeCondition type="or">
                        <condition>
                            <operand>
                                <column alias="Role Parent Group"
                                    heading="Name" propertyName="name" type="java.lang.String">name</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="roleGroups"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">roleGroups</constant>
                            </operand>
                        </condition>
                        <condition>
                            <operand>
                                <column alias="Role" heading="Name"
                                    propertyName="name" type="java.lang.String">name</column>
                            </operand>
                            <operator type="equal"/>
                            <operand>
                                <constant heading="GUEST"
                                    isMacro="false"
                                    type="java.lang.String" xml:space="preserve">GUEST</constant>
                            </operand>
                        </condition>
                    </compositeCondition>
                    <condition>
                        <operand>
                            <column alias="Context"
                                heading="Persist Info.Object Identifier.Classname"
                                propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                        </operand>
                        <operator type="notLike"/>
                        <operand>
                            <constant
                                heading="wt.inf.container.OrgContainer"
                                isMacro="false" type="java.lang.String" xml:space="preserve">wt.inf.container.OrgContainer</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Role Parent Group"/>
                    <aliasTarget alias="Role"/>
                </join>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Role"/>
                    <aliasTarget alias="Role Member level 1"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Role"/>
                    <aliasTarget alias="Context"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Role"/>
                    <aliasTarget alias="Product"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Role"/>
                    <aliasTarget alias="Project (wt.projmgmt.admin.Project2)"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
