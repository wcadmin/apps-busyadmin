<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="false" group="false">
                <object alias="Workflow Template" heading="Master ID" propertyName="master.persistInfo.objectIdentifier.id">
                    <property name="master">
                        <property name="persistInfo">
                            <property name="objectIdentifier">
                                <property name="id"/>
                            </property>
                        </property>
                    </property>
                </object>
                <column alias="Workflow Template" heading="Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Workflow Template"
                    heading="Container Type" type="java.lang.String">master&gt;containerReference.key.classname</column>
                <column alias="Workflow Template" heading="Container ID" type="long">master&gt;containerReference.key.id</column>
                <object alias="Workflow Template"
                    heading="Container Name" propertyName="containerName">
                    <property name="containerName"/>
                </object>
                <object alias="Workflow Template" heading="Enabled" propertyName="enabled">
                    <property name="enabled"/>
                </object>
                <function heading="Master Created" name="LTRIM" type="java.lang.String">
                    <column alias="Workflow Template"
                        heading="master>thePersistInfo.createStamp" type="java.sql.Timestamp">master&gt;thePersistInfo.createStamp</column>
                </function>
                <function heading="Master Modified" name="LTRIM" type="java.lang.String">
                    <column alias="Workflow Template"
                        heading="master>thePersistInfo.modifyStamp" type="java.sql.Timestamp">master&gt;thePersistInfo.modifyStamp</column>
                </function>
                <column alias="Workflow Template" heading="Version ID"
                    propertyName="branchIdentifier" type="long">iterationInfo.branchId</column>
                <object alias="Workflow Template" heading="Version" propertyName="iterationIdentifier.value">
                    <property name="iterationIdentifier">
                        <property name="value"/>
                    </property>
                </object>
                <column alias="Workflow Template" heading="Latest"
                    propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                <column alias="Workflow Template" heading="Category"
                    propertyName="category" type="wt.workflow.definer.WfTemplateCategory">category</column>
                <function heading="Version Created" name="LTRIM" type="java.lang.String">
                    <column alias="Workflow Template" heading="Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <function heading="Version Modified" name="LTRIM" type="java.lang.String">
                    <column alias="Workflow Template"
                        heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
            </select>
            <from>
                <table alias="Workflow Template">wt.workflow.definer.WfProcessTemplate</table>
            </from>
        </query>
    </statement>
</qml>
