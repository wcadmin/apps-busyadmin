<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement rootTypeInstanceAlias="Part">
        <query>
            <select>
                <object alias="Part Master" heading="WTPartMasterID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Part" heading="WTPart Internal ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="Part" heading="Part Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Part" heading="Part Number"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <object alias="Part"
                    heading="Windchill Part CheckedOut By" propertyName="lock.locker.name">
                    <property name="lock">
                        <property name="locker">
                            <property name="name"/>
                        </property>
                    </property>
                </object>
                <column alias="Part" heading="Lock Date"
                    propertyName="lockDate" type="java.sql.Timestamp">lock.date</column>
                <column alias="Part" heading="WTPartVersion" type="java.lang.String">versionInfo.identifier.versionId</column>
                <object alias="Part" heading="Iteration (iterationInfo)" propertyName="iterationDisplayIdentifier">
                    <property name="iterationDisplayIdentifier"/>
                </object>
                <object alias="Part" heading="Folder Path" propertyName="folderPath">
                    <property name="folderPath"/>
                </object>
                <object alias="Part" heading="Domain Reference.Name" propertyName="domainRef.name">
                    <property name="domainRef">
                        <property name="name"/>
                    </property>
                </object>
                <object alias="Part" heading="Cabinet (cabinetName)" propertyName="cabinetName">
                    <property name="cabinetName"/>
                </object>
            </select>
            <from>
                <table alias="Part">wt.part.WTPart</table>
                <table alias="Part Master">wt.part.WTPartMaster</table>
                <table alias="Context 1">wt.inf.container.WTContainer</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <keyword heading="Row Number" name="rownum" type="java.math.BigDecimal"/>
                        </operand>
                        <operator type="lessThanOrEqual"/>
                        <operand>
                            <constant heading="100000" isMacro="false"
                                type="java.math.BigDecimal" xml:space="preserve">100000</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Part"
                                heading="Version (versionInfo).Identifier.Version Sort Id"
                                propertyName="versionInfo.identifier.versionSortId" type="java.lang.String">versionInfo.identifier.versionSortId</column>
                        </operand>
                        <inOperator type="in"/>
                        <inOperand>
                            <subQuery>
                                <subQuerySelect>
                                    <function heading="Maximum"
                                    name="MAX" type="java.lang.String">
                                    <column alias="Part 1"
                                    heading="Version (versionInfo).Identifier.Version Sort Id"
                                    propertyName="versionInfo.identifier.versionSortId" type="java.lang.String">versionInfo.identifier.versionSortId</column>
                                    </function>
                                </subQuerySelect>
                                <from>
                                    <table alias="Part 1">wt.part.WTPart</table>
                                </from>
                                <where>
                                    <compositeCondition type="and">
                                    <condition>
                                    <operand>
                                    <column alias="Part 1"
                                    heading="master>thePersistInfo.theObjectIdentifier.id" type="long">master&gt;thePersistInfo.theObjectIdentifier.id</column>
                                    </operand>
                                    <operator type="equal"/>
                                    <operand>
                                    <column alias="Part"
                                    heading="master>thePersistInfo.theObjectIdentifier.id" type="long">master&gt;thePersistInfo.theObjectIdentifier.id</column>
                                    </operand>
                                    </condition>
                                    </compositeCondition>
                                </where>
                            </subQuery>
                        </inOperand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Context 1"
                                heading="Context Name"
                                propertyName="name" type="java.lang.String">containerInfo.name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Ricardo Parts Library"
                                isMacro="false" type="java.lang.String" xml:space="preserve">Ricardo Parts Library</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Part"
                                heading="Latest Iteration"
                                propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Yes" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">Yes</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <function heading="Get Day" name="GET_DAY" type="java.math.BigDecimal">
                                <column alias="Part" heading="Lock Date"
                                    propertyName="lockDate" type="java.sql.Timestamp">lock.date</column>
                            </function>
                        </operand>
                        <operator type="greaterThan"/>
                        <operand>
                            <constant heading="-1" isMacro="false"
                                type="java.math.BigDecimal" xml:space="preserve">-1</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <referenceJoin>
                <join name="masterReference">
                    <aliasTarget alias="Part"/>
                    <aliasTarget alias="Part Master"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Part Master"/>
                    <aliasTarget alias="Context 1"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
