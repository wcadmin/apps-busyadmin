<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="EPM Document" heading="Context" propertyName="containerName">
                    <property name="containerName"/>
                </object>
                <column alias="Context" heading="Context Name"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="Context"
                    heading="Persist Info.Object Identifier" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Context" heading="Context Type" propertyName="type">
                    <property name="type"/>
                </object>
                <object alias="EPM Document"
                    heading="Persist Info.Object Identifier.Id" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="EPM Document" heading="Number"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <function heading="Family table status" name="DECODE" type="java.lang.String">
                    <column alias="EPM Document"
                        heading="EPM Document Family Table Status"
                        propertyName="familyTableStatus" type="int">familyTableStatus</column>
                    <constant heading="0" type="java.lang.Object" xml:space="preserve">0</constant>
                    <constant heading="NOT RELATED TO FAMILY TABLES"
                        type="java.lang.Object" xml:space="preserve">NOT RELATED TO FAMILY TABLES</constant>
                    <constant heading="1" type="java.lang.Object" xml:space="preserve">1</constant>
                    <constant heading="INSTANCE" type="java.lang.Object" xml:space="preserve">INSTANCE</constant>
                    <constant heading="2" type="java.lang.Object" xml:space="preserve">2</constant>
                    <constant heading="GENERIC" type="java.lang.Object" xml:space="preserve">GENERIC</constant>
                    <constant heading="UNKNOWN" type="java.lang.Object" xml:space="preserve">UNKNOWN</constant>
                </function>
                <object alias="EPM Document" heading="Top Generic" propertyName="topGeneric">
                    <property name="topGeneric"/>
                </object>
                <object alias="EPM Document" heading="Generic" propertyName="generic">
                    <property name="generic"/>
                </object>
                <object alias="EPM Document" heading="Instance" propertyName="instance">
                    <property name="instance"/>
                </object>
                <object alias="EPM Document"
                    heading="Iteration Display Identifier" propertyName="iterationDisplayIdentifier">
                    <property name="iterationDisplayIdentifier"/>
                </object>
                <object alias="EPM Document"
                    heading="EPM Checked Out By" propertyName="lock.locker.name">
                    <property name="lock">
                        <property name="locker">
                            <property name="name"/>
                        </property>
                    </property>
                </object>
                <object alias="EPM Document" heading="Locked" propertyName="locked">
                    <property name="locked"/>
                </object>
                <object alias="EPM Document" heading="Lock" propertyName="lock">
                    <property name="lock"/>
                </object>
                <object alias="EPM Document" heading="Locker Name" propertyName="lockerName">
                    <property name="lockerName"/>
                </object>
                <column alias="EPM Document" heading="Lock Date"
                    propertyName="lockDate" type="java.sql.Timestamp">lock.date</column>
                <column alias="EPM Document" heading="Latest Iteration"
                    propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                <object alias="EPM Document" heading="Folder Path" propertyName="folderPath">
                    <property name="folderPath"/>
                </object>
                <column alias="EPM Document" heading="EPMDocVersion" type="java.lang.String">versionInfo.identifier.versionId</column>
                <object alias="EPM Document"
                    heading="Domain Reference.Name" propertyName="domainRef.name">
                    <property name="domainRef">
                        <property name="name"/>
                    </property>
                </object>
                <object alias="EPM Document"
                    heading="Context (containerName)" propertyName="containerName">
                    <property name="containerName"/>
                </object>
            </select>
            <from>
                <table alias="EPM Document">wt.epm.EPMDocument</table>
                <table alias="Context">wt.inf.container.WTContainer</table>
                <table alias="EPM Document Master">wt.epm.EPMDocumentMaster</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="EPM Document"
                                heading="versionInfo.identifier.versionSortId" type="java.lang.String">versionInfo.identifier.versionSortId</column>
                        </operand>
                        <inOperator type="in"/>
                        <inOperand>
                            <subQuery>
                                <subQuerySelect>
                                    <function heading="Maximum"
                                    name="MAX" type="java.lang.String">
                                    <column alias="EPM Document 1"
                                    heading="versionInfo.identifier.versionSortId" type="java.lang.String">versionInfo.identifier.versionSortId</column>
                                    </function>
                                </subQuerySelect>
                                <from>
                                    <table alias="EPM Document 1">wt.epm.EPMDocument</table>
                                </from>
                                <where>
                                    <compositeCondition type="and">
                                    <condition>
                                    <operand>
                                    <column
                                    alias="EPM Document 1"
                                    heading="master>thePersistInfo.theObjectIdentifier.id" type="long">master&gt;thePersistInfo.theObjectIdentifier.id</column>
                                    </operand>
                                    <operator type="equal"/>
                                    <operand>
                                    <column
                                    alias="EPM Document"
                                    heading="master>thePersistInfo.theObjectIdentifier.id" type="long">master&gt;thePersistInfo.theObjectIdentifier.id</column>
                                    </operand>
                                    </condition>
                                    </compositeCondition>
                                </where>
                            </subQuery>
                        </inOperand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Context"
                                heading="Context Name"
                                propertyName="name" type="java.lang.String">containerInfo.name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Ricardo Parts Library"
                                isMacro="false" type="java.lang.String" xml:space="preserve">Ricardo Parts Library</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="EPM Document"
                                heading="Latest Iteration"
                                propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Yes" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">Yes</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <keyword heading="Row Number" name="rownum" type="java.math.BigDecimal"/>
                        </operand>
                        <operator type="lessThanOrEqual"/>
                        <operand>
                            <constant heading="100000" isMacro="false"
                                type="java.math.BigDecimal" xml:space="preserve">100000</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="EPM Document"
                                heading="Document Category"
                                propertyName="docType" type="wt.epm.EPMDocumentType">master&gt;docType</column>
                        </operand>
                        <operator type="notEqual"/>
                        <operand>
                            <constant heading="CADDRAWING"
                                isMacro="false"
                                type="wt.epm.EPMDocumentType" xml:space="preserve">CADDRAWING</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <function heading="Get Day" name="GET_DAY" type="java.math.BigDecimal">
                                <column alias="EPM Document"
                                    heading="Lock Date"
                                    propertyName="lockDate" type="java.sql.Timestamp">lock.date</column>
                            </function>
                        </operand>
                        <operator type="greaterThan"/>
                        <operand>
                            <constant heading="-1" isMacro="false"
                                type="java.math.BigDecimal" xml:space="preserve">-1</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <referenceJoin>
                <join name="masterReference">
                    <aliasTarget alias="EPM Document"/>
                    <aliasTarget alias="EPM Document Master"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="EPM Document Master"/>
                    <aliasTarget alias="Context"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
