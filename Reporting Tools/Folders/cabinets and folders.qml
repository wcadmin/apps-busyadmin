<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Cabinet" heading="Cabinet Name"
                    propertyName="name" type="java.lang.String">name</column>
                <column alias="Cabinet" heading="Personal Cabinet"
                    propertyName="personalCabinet" type="boolean">personalCabinet</column>
                <column alias="Cabinet" heading="Inherited Domain"
                    propertyName="inheritedDomain" type="boolean">inheritedDomain</column>
                <column alias="Domain of Cabinet"
                    heading="Cabinet Domain" propertyName="name" type="java.lang.String">name</column>
                <object alias="Domain of Cabinet"
                    heading="Context of Domain of Cabinet" propertyName="container">
                    <property name="container"/>
                </object>
                <object alias="Cabinet" heading="Context of Cabinet" propertyName="container">
                    <property name="container"/>
                </object>
                <object alias="User" heading="Owner Name" propertyName="fullName">
                    <property name="fullName"/>
                </object>
                <object alias="User" heading="Owner Dn" propertyName="dn">
                    <property name="dn"/>
                </object>
                <column alias="User" heading="Owner Disabled"
                    propertyName="disabled" type="boolean">disabled</column>
                <object alias="User" heading="Owner Domain" propertyName="domainRef.name">
                    <property name="domainRef">
                        <property name="name"/>
                    </property>
                </object>
                <object alias="User" heading="Owner Organization" propertyName="organization">
                    <property name="organization"/>
                </object>
                <column alias="User" heading="Owner Repair Needed"
                    propertyName="repairNeeded" type="boolean">repairNeeded</column>
                <object alias="User" heading="Owner Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
                <object alias="Folder" heading="Parent Folder" propertyName="parentFolder">
                    <property name="parentFolder"/>
                </object>
                <column alias="Folder" heading="Folder Name"
                    propertyName="name" type="java.lang.String">name</column>
                <object alias="Folder" heading="Folder Path" propertyName="folderPath">
                    <property name="folderPath"/>
                </object>
                <column alias="Folder" heading="Folder Inherited Domain"
                    propertyName="inheritedDomain" type="boolean">inheritedDomain</column>
                <column alias="Domain of Folder"
                    heading="Domain of folder" propertyName="name" type="java.lang.String">name</column>
                <object alias="Domain of Folder"
                    heading="Context of Domain of Folder" propertyName="container">
                    <property name="container"/>
                </object>
                <object alias="Folder" heading="Context of Folder" propertyName="container">
                    <property name="container"/>
                </object>
            </select>
            <from>
                <table alias="Cabinet">wt.folder.Cabinet</table>
                <table alias="Folder" outerJoinAlias="Cabinet">wt.folder.SubFolder</table>
                <table alias="Domain of Cabinet">wt.admin.AdministrativeDomain</table>
                <table alias="Domain of Folder" outerJoinAlias="Folder">wt.admin.AdministrativeDomain</table>
                <table alias="User" outerJoinAlias="Cabinet">wt.org.WTUser</table>
            </from>
            <referenceJoin>
                <join name="cabinet">
                    <aliasTarget alias="Folder"/>
                    <aliasTarget alias="Cabinet"/>
                </join>
                <join name="domainRef">
                    <aliasTarget alias="Cabinet"/>
                    <aliasTarget alias="Domain of Cabinet"/>
                </join>
                <join name="domainRef">
                    <aliasTarget alias="Folder"/>
                    <aliasTarget alias="Domain of Folder"/>
                </join>
                <join name="ownership.owner">
                    <aliasTarget alias="Cabinet"/>
                    <aliasTarget alias="User"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
