<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="EPM Document" heading="Context" propertyName="containerName">
                    <property name="containerName"/>
                </object>
                <object alias="Context" heading="Context Type" propertyName="type">
                    <property name="type"/>
                </object>
                <object alias="EPM Document"
                    heading="Persist Info.Object Identifier.Id" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="EPM Document Master" heading="Number"
                    propertyName="number" type="java.lang.String">number</column>
                <column alias="EPM Document Master" heading="Name"
                    propertyName="name" type="java.lang.String">name</column>
                <column alias="EPM Document Master" heading="File Name"
                    propertyName="CADName" type="java.lang.String">CADName</column>
                <function heading="Family table status" name="DECODE" type="java.lang.String">
                    <column alias="EPM Document"
                        heading="EPM Document Family Table Status"
                        propertyName="familyTableStatus" type="int">familyTableStatus</column>
                    <constant heading="0" type="java.lang.Object" xml:space="preserve">0</constant>
                    <constant heading="NOT RELATED TO FAMILY TABLES"
                        type="java.lang.Object" xml:space="preserve">NOT RELATED TO FAMILY TABLES</constant>
                    <constant heading="1" type="java.lang.Object" xml:space="preserve">1</constant>
                    <constant heading="INSTANCE" type="java.lang.Object" xml:space="preserve">INSTANCE</constant>
                    <constant heading="2" type="java.lang.Object" xml:space="preserve">2</constant>
                    <constant heading="GENERIC" type="java.lang.Object" xml:space="preserve">GENERIC</constant>
                    <constant heading="UNKNOWN" type="java.lang.Object" xml:space="preserve">UNKNOWN</constant>
                </function>
                <object alias="EPM Document" heading="Top Generic" propertyName="topGeneric">
                    <property name="topGeneric"/>
                </object>
                <object alias="EPM Document" heading="Generic" propertyName="generic">
                    <property name="generic"/>
                </object>
                <object alias="EPM Document" heading="Instance" propertyName="instance">
                    <property name="instance"/>
                </object>
                <object alias="EPM Document"
                    heading="Iteration Display Identifier" propertyName="iterationDisplayIdentifier">
                    <property name="iterationDisplayIdentifier"/>
                </object>
                <column alias="EPM Document" heading="Latest Iteration"
                    propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                <object alias="EPM Document" heading="Folder Path" propertyName="folderPath">
                    <property name="folderPath"/>
                </object>
                <function heading="CheckedOutBy" name="CONCAT" type="java.lang.String">
                    <column alias="EPM Document"
                        heading="Lock.Checked Out By.Object Id.Classname"
                        propertyName="lock.locker.objectId.classname" type="java.lang.String">lock.locker.key.classname</column>
                    <column alias="EPM Document"
                        heading="Lock.Checked Out By.Object Id.Id"
                        propertyName="lock.locker.objectId.id" type="long">lock.locker.key.id</column>
                </function>
            </select>
            <from>
                <table alias="EPM Document">wt.epm.EPMDocument</table>
                <table alias="EPM Document Master">wt.epm.EPMDocumentMaster</table>
                <table alias="EPMFamily Table" outerJoinAlias="EPM Document">wt.epm.familytable.EPMFamilyTable</table>
                <table alias="Context">wt.inf.container.WTContainer</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="EPM Document"
                                heading="versionInfo.identifier.versionSortId" type="java.lang.String">versionInfo.identifier.versionSortId</column>
                        </operand>
                        <inOperator type="in"/>
                        <inOperand>
                            <subQuery>
                                <subQuerySelect>
                                    <function heading="Maximum"
                                    name="MAX" type="java.lang.String">
                                    <column alias="EPM Document 1"
                                    heading="versionInfo.identifier.versionSortId" type="java.lang.String">versionInfo.identifier.versionSortId</column>
                                    </function>
                                </subQuerySelect>
                                <from>
                                    <table alias="EPM Document 1">wt.epm.EPMDocument</table>
                                </from>
                                <where>
                                    <compositeCondition type="and">
                                    <condition>
                                    <operand>
                                    <column
                                    alias="EPM Document 1"
                                    heading="master>thePersistInfo.theObjectIdentifier.id" type="long">master&gt;thePersistInfo.theObjectIdentifier.id</column>
                                    </operand>
                                    <operator type="equal"/>
                                    <operand>
                                    <column
                                    alias="EPM Document"
                                    heading="master>thePersistInfo.theObjectIdentifier.id" type="long">master&gt;thePersistInfo.theObjectIdentifier.id</column>
                                    </operand>
                                    </condition>
                                    </compositeCondition>
                                </where>
                            </subQuery>
                        </inOperand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="EPM Document"
                                heading="Latest Iteration"
                                propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Yes" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">Yes</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Number"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.epm.structure.EPMContainedIn">
                    <aliasTarget alias="EPM Document"/>
                    <aliasTarget alias="EPMFamily Table"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="masterReference">
                    <aliasTarget alias="EPM Document"/>
                    <aliasTarget alias="EPM Document Master"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="EPM Document Master"/>
                    <aliasTarget alias="Context"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
