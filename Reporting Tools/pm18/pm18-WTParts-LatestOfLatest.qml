<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="Part" heading="Context (containerName)" propertyName="containerName">
                    <property name="containerName"/>
                </object>
                <column alias="Part" heading="Name" propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Part" heading="Number"
                    propertyName="number" type="java.lang.String">master&gt;number</column>
                <object alias="Part"
                    heading="Persist Info.Object Identifier" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Part"
                    heading="Iteration Display Identifier" propertyName="iterationDisplayIdentifier">
                    <property name="iterationDisplayIdentifier"/>
                </object>
            </select>
            <from>
                <table alias="Part">wt.part.WTPart</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Part"
                                heading="versionInfo.identifier.versionSortId" type="java.lang.String">versionInfo.identifier.versionSortId</column>
                        </operand>
                        <inOperator type="in"/>
                        <inOperand>
                            <subQuery>
                                <subQuerySelect>
                                    <function heading="Maximum"
                                    name="MAX" type="java.lang.String">
                                    <column alias="PartSub"
                                    heading="versionInfo.identifier.versionSortId" type="java.lang.String">versionInfo.identifier.versionSortId</column>
                                    </function>
                                </subQuerySelect>
                                <from>
                                    <table alias="PartSub">wt.part.WTPart</table>
                                </from>
                                <where>
                                    <compositeCondition type="and">
                                    <condition>
                                    <operand>
                                    <column alias="Part"
                                    heading="Master Reference.Object Id.Id"
                                    propertyName="masterReference.objectId.id" type="long">masterReference.key.id</column>
                                    </operand>
                                    <operator type="equal"/>
                                    <operand>
                                    <column alias="PartSub"
                                    heading="Master Reference.Object Id.Id"
                                    propertyName="masterReference.objectId.id" type="long">masterReference.key.id</column>
                                    </operand>
                                    </condition>
                                    </compositeCondition>
                                </where>
                            </subQuery>
                        </inOperand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Part"
                                heading="Latest Iteration"
                                propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Yes" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">Yes</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
        </query>
    </statement>
</qml>
