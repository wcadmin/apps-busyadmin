<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <function heading="Root Type | Soft Type Branch ID"
                    name="DECODE" type="java.lang.String">
                    <function heading="In String" name="IN_STRING" type="java.math.BigDecimal">
                        <column alias="WTType Definition"
                            heading="WTType Definition master>intHid" type="java.lang.String">master&gt;intHid</column>
                        <constant heading="|" type="java.lang.Number" xml:space="preserve">|</constant>
                    </function>
                    <constant heading="0" type="java.lang.Number" xml:space="preserve">0</constant>
                    <column alias="WTType Definition" heading="Name"
                        propertyName="name" type="java.lang.String">name</column>
                    <function heading="Concatenate" name="CONCAT" type="java.lang.String">
                        <function heading="Sub String" name="SUB_STRING" type="java.lang.String">
                            <function heading="Concatenate"
                                name="CONCAT" type="java.lang.String">
                                <column alias="WTType Definition"
                                    heading="WTType Definition master>intHid" type="java.lang.String">master&gt;intHid</column>
                                <constant heading="|"
                                    type="java.lang.Number" xml:space="preserve">|</constant>
                            </function>
                            <constant heading="1"
                                type="java.lang.Number" xml:space="preserve">1</constant>
                            <function heading="In String"
                                name="IN_STRING" type="java.math.BigDecimal">
                                <function heading="Concatenate"
                                    name="CONCAT" type="java.lang.String">
                                    <column alias="WTType Definition"
                                    heading="WTType Definition master>intHid" type="java.lang.String">master&gt;intHid</column>
                                    <constant heading="|"
                                    type="java.lang.Number" xml:space="preserve">|</constant>
                                </function>
                                <constant heading="|"
                                    type="java.lang.Number" xml:space="preserve">|</constant>
                            </function>
                        </function>
                        <column alias="WTType Definition"
                            heading="Branch Identifier"
                            propertyName="branchIdentifier" type="long">iterationInfo.branchId</column>
                    </function>
                </function>
                <column alias="WTType Definition"
                    heading="Full Type Name" type="java.lang.String">master&gt;intHid</column>
            </select>
            <from>
                <table alias="WTType Definition">com.ptc.core.meta.type.mgmt.server.impl.WTTypeDefinition</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="WTType Definition"
                                heading="Latest Iteration"
                                propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="1" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">1</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="WTType Definition"
                                heading="Deleted_id"
                                propertyName="deleted_id" type="java.lang.String">master&gt;deleted_id</column>
                        </operand>
                        <nullOperator type="isNull"/>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Full Type Name"/>
                </orderByItem>
            </orderBy>
        </query>
    </statement>
</qml>
