<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <function heading="Context Name" name="CONCAT" type="java.lang.String">
                    <column alias="Context"
                        heading="Persist Info.Object Identifier.Classname"
                        propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                    <constant heading=" / " type="java.lang.Object" xml:space="preserve"> / </constant>
                    <column alias="Context" heading="Context Name"
                        propertyName="name" type="java.lang.String">containerInfo.name</column>
                </function>
                <object alias="Context" heading="Context ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
            </select>
            <from>
                <table alias="Context">wt.inf.container.WTContainer</table>
            </from>
        </query>
    </statement>
</qml>
