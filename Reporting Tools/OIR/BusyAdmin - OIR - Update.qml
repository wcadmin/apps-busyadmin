<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="Object Initialization Rule"
                    heading="Rule ID" propertyName="identity">
                    <property name="identity"/>
                </object>
                <object alias="Context" heading="Context Type" propertyName="type">
                    <property name="type"/>
                </object>
                <column alias="Product" heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <column alias="Product" heading="Primary CAD System" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|PrimaryCadSystem</column>
                <column alias="Context" heading="Context Name"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <function heading="Modified" name="RTRIM" type="java.lang.String">
                    <column alias="Object Initialization Rule"
                        heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <function heading="Created" name="RTRIM" type="java.lang.String">
                    <column alias="Object Initialization Rule"
                        heading="Created" propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <column alias="Object Initialization Rule"
                    heading="Object Type Branch ID"
                    propertyName="objType" type="java.lang.String">objType</column>
                <column alias="Object Initialization Rule"
                    heading="Current Rule Name" propertyName="name" type="java.lang.String">name</column>
                <column alias="Object Initialization Rule"
                    heading="Current Flag" propertyName="enabledFlag" type="long">enabledFlag</column>
            </select>
            <from>
                <table alias="Object Initialization Rule">wt.rule.TypeBasedRule</table>
                <table alias="Context">wt.inf.container.WTContainer</table>
                <table alias="Product" outerJoinAlias="Object Initialization Rule">wt.pdmlink.PDMLinkProduct</table>
            </from>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Activity Status"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Primary CAD System"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Context Name"/>
                </orderByItem>
            </orderBy>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Object Initialization Rule"/>
                    <aliasTarget alias="Context"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Object Initialization Rule"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
