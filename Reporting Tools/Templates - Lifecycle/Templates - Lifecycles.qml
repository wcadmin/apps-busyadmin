<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <column alias="Life Cycle Template" heading="Master ID" type="long">master&gt;thePersistInfo.theObjectIdentifier.id</column>
                <column alias="Life Cycle Template"
                    heading="Template Name" propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Life Cycle Template" heading="routing" type="boolean">master&gt;routing</column>
                <column alias="Life Cycle Template" heading="Basic"
                    propertyName="basic" type="boolean">basic</column>
                <object alias="Life Cycle Template" heading="Enabled" propertyName="enabled">
                    <property name="enabled"/>
                </object>
                <function heading="master created" name="LTRIM" type="java.lang.String">
                    <column alias="Life Cycle Template"
                        heading="master>thePersistInfo.createStamp" type="java.sql.Timestamp">master&gt;thePersistInfo.createStamp</column>
                </function>
                <function heading="master modified" name="LTRIM" type="java.lang.String">
                    <column alias="Life Cycle Template"
                        heading="master>thePersistInfo.modifyStamp" type="java.sql.Timestamp">master&gt;thePersistInfo.modifyStamp</column>
                </function>
                <column alias="Life Cycle Template"
                    heading="Master container ID" type="long">master&gt;containerReference.key.id</column>
                <column alias="Life Cycle Template"
                    heading="Master container type" type="java.lang.String">master&gt;containerReference.key.classname</column>
                <object alias="Life Cycle Template" heading="Container" propertyName="container">
                    <property name="container"/>
                </object>
                <column alias="Life Cycle Template" heading="Version" type="java.lang.String">iterationInfo.identifier.iterationId</column>
                <column alias="Life Cycle Template" heading="Version ID"
                    propertyName="branchIdentifier" type="long">iterationInfo.branchId</column>
                <column alias="Life Cycle Template"
                    heading="Latest Iteration"
                    propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                <function heading="created" name="LTRIM" type="java.lang.String">
                    <column alias="Life Cycle Template"
                        heading="Created" propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <function heading="Modified" name="LTRIM" type="java.lang.String">
                    <column alias="Life Cycle Template"
                        heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
            </select>
            <from>
                <table alias="Life Cycle Template">wt.lifecycle.LifeCycleTemplate</table>
            </from>
        </query>
    </statement>
</qml>
