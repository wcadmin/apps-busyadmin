<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <column alias="Product" heading="Client" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|Client</column>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Product" heading="Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <column alias="Product" heading="CAD System" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|PrimaryCadSystem</column>
                <function heading="CAD Docs" name="COUNT" type="java.math.BigDecimal">
                    <column alias="CAD Document" heading="Number"
                        propertyName="number" type="java.lang.String">master&gt;number</column>
                </function>
                <function heading="String Mass" name="COUNT" type="java.math.BigDecimal">
                    <column alias="CAD Document"
                        heading="CAD Document R Mass String" type="java.lang.String">WCTYPE|wt.epm.EPMDocument|com.ricardo.DefaultEPMDocument~IBA|R_MASS_STRING</column>
                </function>
                <function heading="Real Mass" name="COUNT" type="java.math.BigDecimal">
                    <column alias="CAD Document" heading="R Mass Real" type="com.ptc.core.meta.common.FloatingPoint">WCTYPE|wt.epm.EPMDocument|com.ricardo.DefaultEPMDocument~IBA|R_MASS_REAL</column>
                </function>
            </select>
            <from>
                <table alias="CAD Document">WCTYPE|wt.epm.EPMDocument|com.ricardo.DefaultEPMDocument</table>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="CAD Document"
                                heading="Latest Iteration"
                                propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="1" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">1</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Client"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Product"/>
                </orderByItem>
            </orderBy>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="CAD Document"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
