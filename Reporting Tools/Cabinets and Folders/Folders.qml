<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <object alias="Cabinet" heading="ID -  Domain - Cabinet" propertyName="domainRef.objectId">
                    <property name="domainRef">
                        <property name="objectId"/>
                    </property>
                </object>
                <function heading="ID - Domain - Folder - Parent"
                    name="DECODE" type="java.lang.String">
                    <column alias="Parent Sub Folder or Cabinet"
                        heading="Persist Info.Object Identifier.Classname"
                        propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                    <constant heading="wt.folder.SubFolder"
                        type="java.lang.Object" xml:space="preserve">wt.folder.SubFolder</constant>
                    <function heading="Concatenate" name="CONCAT" type="java.lang.String">
                        <column alias="Parent Sub Folder"
                            heading="Domain Reference.Object Id.Classname"
                            propertyName="domainRef.objectId.classname" type="java.lang.String">domainRef.key.classname</column>
                        <constant heading=":" type="java.lang.Object" xml:space="preserve">:</constant>
                        <column alias="Parent Sub Folder"
                            heading="Domain Reference.Object Id.Id"
                            propertyName="domainRef.objectId.id" type="long">domainRef.key.id</column>
                    </function>
                    <constant heading="wt.folder.Cabinet"
                        type="java.lang.Object" xml:space="preserve">wt.folder.Cabinet</constant>
                    <function heading="Concatenate" name="CONCAT" type="java.lang.String">
                        <column alias="Cabinet"
                            heading="Domain Reference.Object Id.Classname"
                            propertyName="domainRef.objectId.classname" type="java.lang.String">domainRef.key.classname</column>
                        <constant heading=":" type="java.lang.Object" xml:space="preserve">:</constant>
                        <column alias="Cabinet"
                            heading="Domain Reference.Object Id.Id"
                            propertyName="domainRef.objectId.id" type="long">domainRef.key.id</column>
                    </function>
                </function>
                <object alias="Folder" heading="ID - Domain - Folder" propertyName="domainRef.objectId">
                    <property name="domainRef">
                        <property name="objectId"/>
                    </property>
                </object>
                <column alias="Parent Sub Folder or Cabinet"
                    heading="Type - Folder - Parent"
                    propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                <column alias="Folder"
                    heading="Inherited Domain - Folder"
                    propertyName="inheritedDomain" type="boolean">inheritedDomain</column>
                <column alias="Context" heading="Type - Container"
                    propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                <object alias="Context" heading="ID - Container" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Folder" heading="ID - Folder" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="Context" heading="Name - Container"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="Folder" heading="Path - Folder" propertyName="folderPath">
                    <property name="folderPath"/>
                </object>
                <column alias="Folder" heading="Name - Folder"
                    propertyName="name" type="java.lang.String">name</column>
                <object alias="Parent Sub Folder or Cabinet"
                    heading="Path - Folder - Parent" propertyName="folderPath">
                    <property name="folderPath"/>
                </object>
                <object alias="Parent Sub Folder or Cabinet"
                    heading="Name -  Folder - Parent" propertyName="name">
                    <property name="name"/>
                </object>
                <function heading="Name - Domain - Folder - Parent"
                    name="DECODE" type="java.lang.String">
                    <column alias="Parent Sub Folder or Cabinet"
                        heading="Persist Info.Object Identifier.Classname"
                        propertyName="persistInfo.objectIdentifier.classname" type="java.lang.String">thePersistInfo.theObjectIdentifier.classname</column>
                    <constant heading="wt.folder.SubFolder"
                        type="java.lang.Object" xml:space="preserve">wt.folder.SubFolder</constant>
                    <column alias="Domain - Parent Sub Folder"
                        heading="Name" propertyName="name" type="java.lang.String">name</column>
                    <constant heading="wt.folder.Cabinet"
                        type="java.lang.Object" xml:space="preserve">wt.folder.Cabinet</constant>
                    <column alias="Domain - Cabinet" heading="Name"
                        propertyName="name" type="java.lang.String">name</column>
                </function>
                <object alias="Folder" heading="Name -  Domain - Folder" propertyName="domainRef.name">
                    <property name="domainRef">
                        <property name="name"/>
                    </property>
                </object>
                <column alias="Product" heading="Product - Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <column alias="Product" heading="Product - CAD" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|PrimaryCadSystem</column>
            </select>
            <from>
                <table alias="Folder">wt.folder.SubFolder</table>
                <table alias="Context">wt.inf.container.WTContainer</table>
                <table alias="Parent Sub Folder" outerJoinAlias="Folder">wt.folder.SubFolder</table>
                <table alias="Cabinet">wt.folder.Cabinet</table>
                <table alias="Parent Sub Folder or Cabinet">wt.folder.Folder</table>
                <table alias="Domain - Parent Sub Folder" outerJoinAlias="Parent Sub Folder">wt.admin.AdministrativeDomain</table>
                <table alias="Domain - Cabinet">wt.admin.AdministrativeDomain</table>
                <table alias="Product" outerJoinAlias="Folder">wt.pdmlink.PDMLinkProduct</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Folder"
                                heading="Context (containerReference).Object Id.Classname"
                                propertyName="containerReference.objectId.classname" type="java.lang.String">containerReference.key.classname</column>
                        </operand>
                        <operator type="notLike"/>
                        <operand>
                            <constant
                                heading="wt.inf.container.ExchangeContainer"
                                isMacro="false" type="java.lang.String" xml:space="preserve">wt.inf.container.ExchangeContainer</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Folder"
                                heading="Context (containerReference).Object Id.Classname"
                                propertyName="containerReference.objectId.classname" type="java.lang.String">containerReference.key.classname</column>
                        </operand>
                        <operator type="notLike"/>
                        <operand>
                            <constant
                                heading="wt.inf.container.OrgContainer"
                                isMacro="false" type="java.lang.String" xml:space="preserve">wt.inf.container.OrgContainer</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.folder.FolderMemberLink">
                    <aliasTarget alias="Parent Sub Folder or Cabinet"/>
                    <aliasTarget alias="Folder"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Folder"/>
                    <aliasTarget alias="Context"/>
                </join>
                <join name="parentFolder">
                    <aliasTarget alias="Folder"/>
                    <aliasTarget alias="Parent Sub Folder"/>
                </join>
                <join name="defaultCabinetReference">
                    <aliasTarget alias="Context"/>
                    <aliasTarget alias="Cabinet"/>
                </join>
                <join name="domainRef">
                    <aliasTarget alias="Parent Sub Folder"/>
                    <aliasTarget alias="Domain - Parent Sub Folder"/>
                </join>
                <join name="domainRef">
                    <aliasTarget alias="Cabinet"/>
                    <aliasTarget alias="Domain - Cabinet"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Folder"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
