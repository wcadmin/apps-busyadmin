<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <function heading="Model Number" name="CONCAT" type="java.lang.String">
                    <constant heading="'" type="java.lang.Object" xml:space="preserve">'</constant>
                    <column alias="Model" heading="Model Number"
                        propertyName="number" type="java.lang.String">master&gt;number</column>
                </function>
                <function heading="Drawing Number" name="CONCAT" type="java.lang.String">
                    <constant heading="'" type="java.lang.Object" xml:space="preserve">'</constant>
                    <column alias="Drawing" heading="Drawing Number"
                        propertyName="number" type="java.lang.String">master&gt;number</column>
                </function>
                <column alias="Drawing" heading="Drawing Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Drawing" heading="Drawing File Name"
                    propertyName="CADName" type="java.lang.String">master&gt;CADName</column>
                <function heading="Drawing Created" name="LTRIM" type="java.lang.String">
                    <column alias="Drawing" heading="Drawing Created 1"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <object alias="Drawing" heading="Drawing Created By" propertyName="creatorFullName">
                    <property name="creatorFullName"/>
                </object>
                <column alias="Drawing" heading="Drawing State"
                    propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                <object alias="Drawing" heading="Drawing Version" propertyName="iterationDisplayIdentifier">
                    <property name="iterationDisplayIdentifier"/>
                </object>
                <function heading="Drawing Modified" name="LTRIM" type="java.lang.String">
                    <column alias="Drawing" heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <object alias="Drawing" heading="Drawing Modified By" propertyName="modifierFullName">
                    <property name="modifierFullName"/>
                </object>
            </select>
            <from>
                <table alias="Drawing" outerJoinAlias="EPM Document Reference Link">WCTYPE|wt.epm.EPMDocument|com.ricardo.DefaultEPMDocument</table>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="Model Master" outerJoinAlias="Model">WCTYPE|wt.epm.EPMDocumentMaster|com.ricardo.DefaultEPMDocumentMaster</table>
                <table alias="EPM Document Reference Link" outerJoinAlias="Model Master">wt.epm.structure.EPMReferenceLink</table>
                <table alias="Model">WCTYPE|wt.epm.EPMDocument|com.ricardo.DefaultEPMDocument</table>
                <table alias="Part">wt.part.WTPart</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Context Name"
                                propertyName="name" type="java.lang.String">containerInfo.name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="C008323" isMacro="false"
                                type="java.lang.String" xml:space="preserve">C008323</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="EPM Document Reference Link"
                                heading="Reference Type"
                                propertyName="referenceType" type="wt.epm.structure.EPMReferenceType">referenceType</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="DRAWING" isMacro="false"
                                type="wt.epm.structure.EPMReferenceType" xml:space="preserve">DRAWING</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Drawing"
                                heading="Latest Iteration"
                                propertyName="latestIteration" type="boolean">iterationInfo.latest</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="yes" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">yes</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.build.BuildRule">
                    <aliasTarget alias="Model"/>
                    <aliasTarget alias="Part"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="roleBObjectRef">
                    <aliasTarget alias="EPM Document Reference Link"/>
                    <aliasTarget alias="Model Master"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="EPM Document Reference Link"/>
                    <aliasTarget alias="Drawing"/>
                </join>
                <join name="masterReference">
                    <aliasTarget alias="Model"/>
                    <aliasTarget alias="Model Master"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Part"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
