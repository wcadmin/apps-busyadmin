<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <function heading="Part Number" name="CONCAT" type="java.lang.String">
                    <constant heading="'" type="java.lang.Object" xml:space="preserve">'</constant>
                    <column alias="Part" heading="Part Number"
                        propertyName="number" type="java.lang.String">master&gt;number</column>
                </function>
                <object alias="Part" heading="Part Version" propertyName="iterationDisplayIdentifier">
                    <property name="iterationDisplayIdentifier"/>
                </object>
                <function heading="Model Number" name="CONCAT" type="java.lang.String">
                    <constant heading="'" type="java.lang.Object" xml:space="preserve">'</constant>
                    <column alias="Model" heading="Model Number"
                        propertyName="number" type="java.lang.String">master&gt;number</column>
                </function>
                <column alias="Model" heading=" Model Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Model" heading="Model Filename"
                    propertyName="CADName" type="java.lang.String">master&gt;CADName</column>
                <function heading="Model Created" name="LTRIM" type="java.lang.String">
                    <column alias="Model" heading="Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <object alias="Model" heading="Model Created By" propertyName="creatorFullName">
                    <property name="creatorFullName"/>
                </object>
                <column alias="Model" heading="Model State"
                    propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                <object alias="Model" heading="Model Version" propertyName="iterationDisplayIdentifier">
                    <property name="iterationDisplayIdentifier"/>
                </object>
                <column alias="Model" heading="Model Mass" type="java.lang.String">WCTYPE|wt.epm.EPMDocument|com.ricardo.DefaultEPMDocument~IBA|R_MASS_STRING</column>
                <function heading="Model Modified" name="LTRIM" type="java.lang.String">
                    <column alias="Model" heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <object alias="Model" heading="Model Modified By" propertyName="modifierFullName">
                    <property name="modifierFullName"/>
                </object>
            </select>
            <from>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="Model">WCTYPE|wt.epm.EPMDocument|com.ricardo.DefaultEPMDocument</table>
                <table alias="Part">wt.part.WTPart</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Context Name"
                                propertyName="name" type="java.lang.String">containerInfo.name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="C008323" isMacro="false"
                                type="java.lang.String" xml:space="preserve">C008323</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <linkJoin>
                <join name="wt.build.BuildRule">
                    <aliasTarget alias="Model"/>
                    <aliasTarget alias="Part"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Part"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
