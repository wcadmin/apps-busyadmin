<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Product" heading="Client" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|Client</column>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Part Master" heading="End Item Number"
                    propertyName="number" type="java.lang.String">number</column>
                <column alias="Part Master" heading="End Item Name"
                    propertyName="name" type="java.lang.String">name</column>
            </select>
            <from>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="Part Master">wt.part.WTPartMaster</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Part Master"
                                heading="End Item"
                                propertyName="endItem" type="boolean">endItem</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="1" isMacro="false"
                                type="java.lang.Object" xml:space="preserve">1</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Client"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Product"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="End Item Number"/>
                </orderByItem>
            </orderBy>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Part Master"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
