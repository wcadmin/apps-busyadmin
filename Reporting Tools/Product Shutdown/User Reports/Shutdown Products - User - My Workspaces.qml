<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="false" group="true">
                <column alias="Product" heading="WS Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="EPMWorkspace" heading="WS Name" propertyName=""/>
                <function heading="WS Created (days ago)" name="ROUND" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="EPMWorkspace"
                            heading="WS Created"
                            propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                    </function>
                    <constant heading="3" type="java.lang.Object" xml:space="preserve">3</constant>
                </function>
                <function heading="WS Used (days ago)" name="ROUND" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="EPMCheckpoint" heading="WS Used"
                            propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                    </function>
                    <constant heading="3" type="java.lang.Object" xml:space="preserve">3</constant>
                </function>
                <function heading="Items Count" name="COUNT" type="java.math.BigDecimal">
                    <column alias="Versioned"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                </function>
                <function heading="Part Count (checkout)" name="COUNT" type="java.math.BigDecimal">
                    <column alias="Part"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                </function>
                <function heading="CAD Count (modified)" name="COUNT" type="java.math.BigDecimal">
                    <column alias="EPM Document"
                        heading="Persist Info.Object Identifier.Id"
                        propertyName="persistInfo.objectIdentifier.id" type="long">thePersistInfo.theObjectIdentifier.id</column>
                </function>
            </select>
            <from>
                <table alias="EPMWorkspace">wt.epm.workspaces.EPMWorkspace</table>
                <table alias="User">wt.org.WTUser</table>
                <table alias="Product" outerJoinAlias="EPMWorkspace">wt.pdmlink.PDMLinkProduct</table>
                <table alias="EPMCheckpoint" outerJoinAlias="EPMWorkspace">wt.epm.workspaces.EPMCheckpoint</table>
                <table alias="Baseline Member" outerJoinAlias="EPMCheckpoint">wt.vc.baseline.BaselineMember</table>
                <table alias="Versioned" outerJoinAlias="Baseline Member">wt.vc.Versioned</table>
                <table alias="Checkout Link" outerJoinAlias="Versioned">wt.vc.wip.CheckoutLink</table>
                <table alias="EPM Document" outerJoinAlias="Checkout Link">wt.epm.EPMDocument</table>
                <table alias="Part" outerJoinAlias="Checkout Link">wt.part.WTPart</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="like"/>
                        <operand>
                            <constant heading="Passive" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Passive</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="User" heading="Name"
                                propertyName="name" type="java.lang.String">name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="CURRENT_USER_NAME"
                                isMacro="true" type="java.lang.String" xml:space="preserve">CURRENT_USER_NAME</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="WS Product"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.epm.workspaces.WorkspaceContainer">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="Product"/>
                </join>
                <join name="wt.epm.workspaces.WorkspaceCheckpoint">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="EPMCheckpoint"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="principalReference">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="User"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Baseline Member"/>
                    <aliasTarget alias="EPMCheckpoint"/>
                </join>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Baseline Member"/>
                    <aliasTarget alias="Versioned"/>
                </join>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Versioned"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="EPM Document"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Part"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
