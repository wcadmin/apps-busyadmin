<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <column alias="Product EPM" heading="CAD Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Product workspace" heading="WS Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Product workspace"
                    heading="WS Product Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <object alias="EPMWorkspace" heading="WS Name" propertyName=""/>
                <column alias="EPM Document origional"
                    heading="CAD Status"
                    propertyName="checkoutInfo.state" type="wt.vc.wip.WorkInProgressState">checkoutInfo.state</column>
                <function heading="WS days since created" name="ROUND" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="EPMWorkspace"
                            heading="WS Created"
                            propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                    </function>
                    <constant heading="3" type="java.lang.Object" xml:space="preserve">3</constant>
                </function>
                <function heading="WS days since used" name="ROUND" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="EPMCheckpoint" heading="WS Used"
                            propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                    </function>
                    <constant heading="3" type="java.lang.Object" xml:space="preserve">3</constant>
                </function>
                <object alias="EPM Document working"
                    heading="CAD Document" propertyName=""/>
                <function heading="CAD Last Checkin (days ago)"
                    name="ROUND" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="EPM Document origional"
                            heading="Last Modified"
                            propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                    </function>
                    <constant heading="3" type="java.lang.Object" xml:space="preserve">3</constant>
                </function>
                <function heading="Working First Mod (days ago)"
                    name="ROUND" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="Checkout Link" heading="Created"
                            propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                    </function>
                    <constant heading="3" type="java.lang.Object" xml:space="preserve">3</constant>
                </function>
                <function heading="Working Last Mod (days ago)"
                    name="ROUND" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="EPM Document working"
                            heading="Last Modified"
                            propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                    </function>
                    <constant heading="3" type="java.lang.Object" xml:space="preserve">3</constant>
                </function>
            </select>
            <from>
                <table alias="EPMWorkspace">wt.epm.workspaces.EPMWorkspace</table>
                <table alias="User">wt.org.WTUser</table>
                <table alias="Product workspace">wt.pdmlink.PDMLinkProduct</table>
                <table alias="EPMCheckpoint">wt.epm.workspaces.EPMCheckpoint</table>
                <table alias="EPM Document working">wt.epm.EPMDocument</table>
                <table alias="EPM Document origional">wt.epm.EPMDocument</table>
                <table alias="Checkout Link">wt.vc.wip.CheckoutLink</table>
                <table alias="Product EPM">wt.pdmlink.PDMLinkProduct</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product EPM"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="like"/>
                        <operand>
                            <constant heading="Passive" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Passive</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="User" heading="Name"
                                propertyName="name" type="java.lang.String">name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="CURRENT_USER_NAME"
                                isMacro="true" type="java.lang.String" xml:space="preserve">CURRENT_USER_NAME</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="CAD Product"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="WS Product"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.epm.workspaces.WorkspaceContainer">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="Product workspace"/>
                </join>
                <join name="wt.epm.workspaces.WorkspaceCheckpoint">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="EPMCheckpoint"/>
                </join>
                <join name="wt.vc.baseline.BaselineMember">
                    <aliasTarget alias="EPMCheckpoint"/>
                    <aliasTarget alias="EPM Document working"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="principalReference">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="User"/>
                </join>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="EPM Document working"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="EPM Document origional"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="EPM Document origional"/>
                    <aliasTarget alias="Product EPM"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
