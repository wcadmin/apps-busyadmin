<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="false" group="false">
                <column alias="Role" heading="Role" propertyName="name" type="java.lang.String">name</column>
                <object alias="Product" heading="Product Link" propertyName=""/>
            </select>
            <from>
                <table alias="Role Group" isExternal="false">
					wt.org.WTGroup
				</table>
                <table alias="Role">wt.org.WTGroup</table>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="User">wt.org.WTUser</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="like"/>
                        <operand>
                            <constant heading="Passive" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Passive</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="User" heading="Name"
                                propertyName="name" type="java.lang.String">name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="CURRENT_USER_NAME"
                                isMacro="true" type="java.lang.String" xml:space="preserve">CURRENT_USER_NAME</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Role Group" heading="Name"
                                propertyName="name" type="java.lang.String">name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="roleGroups"
                                isMacro="false" type="java.lang.String" xml:space="preserve">roleGroups</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Role"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Role Group"/>
                    <aliasTarget alias="Role"/>
                </join>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Role"/>
                    <aliasTarget alias="User"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Role"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
