<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="Part origional" heading="Part" propertyName=""/>
                <object alias="EPMWorkspace" heading="Workspace Name" propertyName=""/>
            </select>
            <from>
                <table alias="Checkout Link">wt.vc.wip.CheckoutLink</table>
                <table alias="Part working">wt.part.WTPart</table>
                <table alias="EPMCheckpoint" outerJoinAlias="Part working">wt.epm.workspaces.EPMCheckpoint</table>
                <table alias="EPMWorkspace" outerJoinAlias="EPMCheckpoint">wt.epm.workspaces.EPMWorkspace</table>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="Part origional">wt.part.WTPart</table>
                <table alias="User">wt.org.WTUser</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="User" heading="Name"
                                propertyName="name" type="java.lang.String">name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="CURRENT_USER_NAME"
                                isMacro="true" type="java.lang.String" xml:space="preserve">CURRENT_USER_NAME</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="like"/>
                        <operand>
                            <constant heading="Passive" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Passive</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Product"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.vc.baseline.BaselineMember">
                    <aliasTarget alias="EPMCheckpoint"/>
                    <aliasTarget alias="Part working"/>
                </join>
                <join name="wt.epm.workspaces.WorkspaceCheckpoint">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="EPMCheckpoint"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Part working"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Part working"/>
                    <aliasTarget alias="Product"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Part origional"/>
                </join>
                <join name="lock.locker">
                    <aliasTarget alias="Part origional"/>
                    <aliasTarget alias="User"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
