<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="false" group="false">
                <object alias="ECN" heading="ECN Number" propertyName=""/>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="ECN" heading="ECN State"
                    propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                <column alias="ECN" heading="ECN Name"
                    propertyName="name" type="java.lang.String">master&gt;name</column>
                <column alias="Work Item" heading="Task Status"
                    propertyName="status" type="wt.workflow.work.WfAssignmentState">status</column>
                <object alias="Work Item" heading="Task Name" propertyName=""/>
                <column alias="Work Item" heading="Task Required"
                    propertyName="required" type="boolean">required</column>
                <column alias="Work Item" heading="Task Role"
                    propertyName="role" type="wt.project.Role">role</column>
                <object alias="Work Item" heading="Task Assignee" propertyName="ownership.owner.name">
                    <property name="ownership">
                        <property name="owner">
                            <property name="name"/>
                        </property>
                    </property>
                </object>
                <function heading="Task Age (days)" name="ROUND" type="java.math.BigDecimal">
                    <function heading="Time Difference(days)"
                        name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                        <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                        <column alias="Work Item"
                            heading="Work Item Created"
                            propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                    </function>
                    <constant heading="3" type="java.lang.Object" xml:space="preserve">3</constant>
                </function>
            </select>
            <from>
                <table alias="Role Group" isExternal="false">
					wt.org.WTGroup
				</table>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="Role">wt.org.WTGroup</table>
                <table alias="User" outerJoinAlias="Work Item">wt.org.WTUser</table>
                <table alias="ECN">wt.change2.WTChangeOrder2</table>
                <table alias="Work Item" outerJoinAlias="ECN">wt.workflow.work.WorkItem</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="like"/>
                        <operand>
                            <constant heading="Passive" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Passive</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="User" heading="Name"
                                propertyName="name" type="java.lang.String">name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="CURRENT_USER_NAME"
                                isMacro="true" type="java.lang.String" xml:space="preserve">CURRENT_USER_NAME</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Role Group" heading="Name"
                                propertyName="name" type="java.lang.String">name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="roleGroups"
                                isMacro="false" type="java.lang.String" xml:space="preserve">roleGroups</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="ECN" heading="State"
                                propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                        </operand>
                        <inOperator type="notIn"/>
                        <inOperand>
                            <delimitedList>
                                <constant heading="RESOLVED"
                                    isMacro="false"
                                    type="wt.lifecycle.State" xml:space="preserve">RESOLVED</constant>
                            </delimitedList>
                        </inOperand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="ECN" heading="State"
                                propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                        </operand>
                        <inOperator type="notIn"/>
                        <inOperand>
                            <delimitedList>
                                <constant heading="CANCELLED"
                                    isMacro="false"
                                    type="wt.lifecycle.State" xml:space="preserve">CANCELLED</constant>
                            </delimitedList>
                        </inOperand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Product"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="ECN State"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="ECN Name"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Task Status"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Task Required"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Task Role"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Role Group"/>
                    <aliasTarget alias="Role"/>
                </join>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Role"/>
                    <aliasTarget alias="User"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Role Group"/>
                    <aliasTarget alias="Product"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="ECN"/>
                    <aliasTarget alias="Product"/>
                </join>
                <join name="primaryBusinessObject">
                    <aliasTarget alias="Work Item"/>
                    <aliasTarget alias="ECN"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
