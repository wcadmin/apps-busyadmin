<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="false" group="false">
                <column alias="Product" heading="Product Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <object alias="Product" heading="Product Link" propertyName=""/>
                <column alias="Product" heading="Product Name"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Role" heading="Role" propertyName="name" type="java.lang.String">name</column>
                <column alias="User" heading="Disabled"
                    propertyName="disabled" type="boolean">disabled</column>
                <column alias="User" heading="Name" propertyName="name" type="java.lang.String">name</column>
                <column alias="User" heading="Full Name"
                    propertyName="fullName" type="java.lang.String">fullName</column>
                <column alias="User" heading="Email"
                    propertyName="eMail" type="java.lang.String">eMail</column>
                <object alias="User" heading="Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
                <object alias="User" heading="User ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Product" heading="Product ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Role" heading="Role ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
            </select>
            <from>
                <table alias="Role Group" isExternal="false">
					wt.org.WTGroup
				</table>
                <table alias="Role">wt.org.WTGroup</table>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="User">wt.org.WTUser</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Role Group" heading="Name"
                                propertyName="name" type="java.lang.String">name</column>
                        </operand>
                        <operator type="equal"/>
                        <operand>
                            <constant heading="roleGroups"
                                isMacro="false" type="java.lang.String" xml:space="preserve">roleGroups</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="notLike"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Product Status"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Product Name"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Role"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Disabled"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Name"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Role Group"/>
                    <aliasTarget alias="Role"/>
                </join>
                <join name="wt.org.MembershipLink">
                    <aliasTarget alias="Role"/>
                    <aliasTarget alias="User"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="Role"/>
                    <aliasTarget alias="Product"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
