<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="false" group="false">
                <column alias="Product" heading="Product Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="PN" heading="PN Link" propertyName=""/>
                <column alias="PN" heading="PN Number"
                    propertyName="number" type="java.lang.String">number</column>
                <column alias="PN" heading="PN Name" propertyName="name" type="java.lang.String">name</column>
                <column alias="PN" heading="PN State"
                    propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                <column alias="Work Item" heading="Task Status"
                    propertyName="status" type="wt.workflow.work.WfAssignmentState">status</column>
                <object alias="Work Item" heading="Task Item" propertyName=""/>
                <column alias="Work Item" heading="Task Role"
                    propertyName="role" type="wt.project.Role">role</column>
                <column alias="Work Item" heading="Task Reassigned"
                    propertyName="reassigned" type="boolean">reassigned</column>
                <column alias="Work Item" heading="Task Required"
                    propertyName="required" type="boolean">required</column>
                <function heading="Task Age (days)"
                    name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                    <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                    <column alias="Work Item"
                        heading="Work Item Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <column alias="User" heading="Name" propertyName="name" type="java.lang.String">name</column>
                <column alias="User" heading="Full Name"
                    propertyName="fullName" type="java.lang.String">fullName</column>
                <column alias="User" heading="Email"
                    propertyName="eMail" type="java.lang.String">eMail</column>
                <column alias="User" heading="Disabled"
                    propertyName="disabled" type="boolean">disabled</column>
                <object alias="User" heading="Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
                <object alias="User" heading="User ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Product" heading="Product ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
            </select>
            <from>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="User" outerJoinAlias="Work Item">wt.org.WTUser</table>
                <table alias="Work Item" outerJoinAlias="PN">wt.workflow.work.WorkItem</table>
                <table alias="PN">wt.maturity.PromotionNotice</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="notLike"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="PN" heading="State"
                                propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                        </operand>
                        <inOperator type="notIn"/>
                        <inOperand>
                            <delimitedList>
                                <constant heading="APPROVED"
                                    isMacro="false"
                                    type="wt.lifecycle.State" xml:space="preserve">APPROVED</constant>
                            </delimitedList>
                        </inOperand>
                    </condition>
                    <condition>
                        <operand>
                            <column alias="PN" heading="State"
                                propertyName="lifeCycleState" type="wt.lifecycle.State">state.state</column>
                        </operand>
                        <inOperator type="notIn"/>
                        <inOperand>
                            <delimitedList>
                                <constant heading="REJECTED"
                                    isMacro="false"
                                    type="wt.lifecycle.State" xml:space="preserve">REJECTED</constant>
                            </delimitedList>
                        </inOperand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Product Status"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Product"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="PN Number"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Task Status"/>
                </orderByItem>
            </orderBy>
            <referenceJoin>
                <join name="primaryBusinessObject">
                    <aliasTarget alias="Work Item"/>
                    <aliasTarget alias="PN"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="PN"/>
                    <aliasTarget alias="Product"/>
                </join>
                <join name="ownership.owner">
                    <aliasTarget alias="Work Item"/>
                    <aliasTarget alias="User"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
