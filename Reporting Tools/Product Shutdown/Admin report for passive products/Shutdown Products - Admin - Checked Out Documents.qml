<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Product" heading="Product Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="Document origional" heading="Document" propertyName=""/>
                <object alias="Document origional" heading="Type" propertyName="displayType">
                    <property name="displayType"/>
                </object>
                <column alias="User" heading="Name" propertyName="name" type="java.lang.String">name</column>
                <column alias="User" heading="Full Name"
                    propertyName="fullName" type="java.lang.String">fullName</column>
                <column alias="User" heading="Email"
                    propertyName="eMail" type="java.lang.String">eMail</column>
                <column alias="User" heading="Disabled"
                    propertyName="disabled" type="boolean">disabled</column>
                <object alias="User" heading="Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
                <object alias="User" heading="User ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
            </select>
            <from>
                <table alias="Checkout Link">wt.vc.wip.CheckoutLink</table>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="User">wt.org.WTUser</table>
                <table alias="Document working">wt.doc.WTDocument</table>
                <table alias="Document origional">wt.doc.WTDocument</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="notLike"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <referenceJoin>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Document working"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Document working"/>
                    <aliasTarget alias="Product"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Document origional"/>
                </join>
                <join name="lock.locker">
                    <aliasTarget alias="Document origional"/>
                    <aliasTarget alias="User"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
