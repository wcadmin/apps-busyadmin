<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml bypassAccessControl="true">
    <statement>
        <query>
            <select distinct="true" group="true">
                <column alias="Product EPM" heading="CAD Product Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <column alias="Product EPM" heading="CAD Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <column alias="Product workspace"
                    heading="WS Product Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <column alias="Product workspace" heading="WS Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="EPMWorkspace" heading="WS Name" propertyName=""/>
                <function heading="WS Created (days ago)"
                    name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                    <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                    <column alias="EPMWorkspace" heading="WS Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <function heading="WS Used (days ago)"
                    name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                    <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                    <column alias="EPMCheckpoint" heading="WS Used"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <column alias="EPM Document origional"
                    heading="CAD Status"
                    propertyName="checkoutInfo.state" type="wt.vc.wip.WorkInProgressState">checkoutInfo.state</column>
                <function heading="CAD Last Checkin (days ago)"
                    name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                    <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                    <column alias="EPM Document origional"
                        heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                </function>
                <function heading="Version First Mod (days ago)"
                    name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                    <function heading="System Date" name="SYSDATE" type="java.util.Date"/>
                    <column alias="Checkout Link" heading="Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <function heading="Version Last Mod (days ago)"
                    name="TIME_DIFFERENCE_IN_DAY" type="java.math.BigDecimal">
                    <column alias="EPM Document working"
                        heading="Last Modified"
                        propertyName="modifyTimestamp" type="java.sql.Timestamp">thePersistInfo.modifyStamp</column>
                    <column alias="Checkout Link" heading="Created"
                        propertyName="createTimestamp" type="java.sql.Timestamp">thePersistInfo.createStamp</column>
                </function>
                <object alias="EPM Document working"
                    heading="CAD Document" propertyName=""/>
                <column alias="User" heading="Name" propertyName="name" type="java.lang.String">name</column>
                <column alias="User" heading="Full Name"
                    propertyName="fullName" type="java.lang.String">fullName</column>
                <column alias="User" heading="Email"
                    propertyName="eMail" type="java.lang.String">eMail</column>
                <column alias="User" heading="Disabled"
                    propertyName="disabled" type="boolean">disabled</column>
                <object alias="User" heading="Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
                <object alias="EPMWorkspace" heading="Workspace ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="User" heading="User ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="Product EPM" heading="Product ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
            </select>
            <from>
                <table alias="EPMWorkspace">wt.epm.workspaces.EPMWorkspace</table>
                <table alias="Product workspace">wt.pdmlink.PDMLinkProduct</table>
                <table alias="EPMCheckpoint">wt.epm.workspaces.EPMCheckpoint</table>
                <table alias="EPM Document working">wt.epm.EPMDocument</table>
                <table alias="EPM Document origional">wt.epm.EPMDocument</table>
                <table alias="Checkout Link">wt.vc.wip.CheckoutLink</table>
                <table alias="Product EPM">wt.pdmlink.PDMLinkProduct</table>
                <table alias="User">wt.org.WTUser</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product EPM"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="notLike"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="CAD Product Status"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="CAD Product"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.epm.workspaces.WorkspaceContainer">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="Product workspace"/>
                </join>
                <join name="wt.epm.workspaces.WorkspaceCheckpoint">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="EPMCheckpoint"/>
                </join>
                <join name="wt.vc.baseline.BaselineMember">
                    <aliasTarget alias="EPMCheckpoint"/>
                    <aliasTarget alias="EPM Document working"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="EPM Document working"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="EPM Document origional"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="EPM Document origional"/>
                    <aliasTarget alias="Product EPM"/>
                </join>
                <join name="principalReference">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="User"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
