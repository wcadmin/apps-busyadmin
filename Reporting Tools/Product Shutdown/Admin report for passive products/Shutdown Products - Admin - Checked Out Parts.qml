<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <statement>
        <query>
            <select>
                <column alias="Product" heading="Product Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                <column alias="Product" heading="Product"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="EPMWorkspace" heading="EPMWorkspace" propertyName=""/>
                <object alias="Part origional" heading="Part" propertyName=""/>
                <column alias="User" heading="Disabled"
                    propertyName="disabled" type="boolean">disabled</column>
                <column alias="User" heading="Email"
                    propertyName="eMail" type="java.lang.String">eMail</column>
                <column alias="User" heading="Full Name"
                    propertyName="fullName" type="java.lang.String">fullName</column>
                <column alias="User" heading="Name" propertyName="name" type="java.lang.String">name</column>
                <object alias="User" heading="Repository" propertyName="repository">
                    <property name="repository"/>
                </object>
                <object alias="Product" heading="Product ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="EPMWorkspace" heading="Workspace ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <object alias="User" heading="User ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
            </select>
            <from>
                <table alias="Checkout Link">wt.vc.wip.CheckoutLink</table>
                <table alias="Part working">wt.part.WTPart</table>
                <table alias="EPMCheckpoint" outerJoinAlias="Part working">wt.epm.workspaces.EPMCheckpoint</table>
                <table alias="EPMWorkspace" outerJoinAlias="EPMCheckpoint">wt.epm.workspaces.EPMWorkspace</table>
                <table alias="Product">wt.pdmlink.PDMLinkProduct</table>
                <table alias="Part origional">wt.part.WTPart</table>
                <table alias="User">wt.org.WTUser</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Product"
                                heading="Activity Status" type="java.lang.String">WCTYPE|wt.pdmlink.PDMLinkProduct~IBA|ActivityStatus</column>
                        </operand>
                        <operator type="notLike"/>
                        <operand>
                            <constant heading="Active" isMacro="false"
                                type="java.lang.String" xml:space="preserve">Active</constant>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <orderBy>
                <orderByItem>
                    <columnTarget heading="Product Status"/>
                </orderByItem>
                <orderByItem>
                    <columnTarget heading="Product"/>
                </orderByItem>
            </orderBy>
            <linkJoin>
                <join name="wt.vc.baseline.BaselineMember">
                    <aliasTarget alias="EPMCheckpoint"/>
                    <aliasTarget alias="Part working"/>
                </join>
                <join name="wt.epm.workspaces.WorkspaceCheckpoint">
                    <aliasTarget alias="EPMWorkspace"/>
                    <aliasTarget alias="EPMCheckpoint"/>
                </join>
            </linkJoin>
            <referenceJoin>
                <join name="roleBObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Part working"/>
                </join>
                <join name="containerReference">
                    <aliasTarget alias="Part working"/>
                    <aliasTarget alias="Product"/>
                </join>
                <join name="roleAObjectRef">
                    <aliasTarget alias="Checkout Link"/>
                    <aliasTarget alias="Part origional"/>
                </join>
                <join name="lock.locker">
                    <aliasTarget alias="Part origional"/>
                    <aliasTarget alias="User"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
