<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE qml SYSTEM "/wt/query/qml/qml.dtd">
<qml>
    <parameter name="Context Name" type="java.lang.String"/>
    <statement>
        <query>
            <select>
                <column alias="Context" heading="Context"
                    propertyName="name" type="java.lang.String">containerInfo.name</column>
                <object alias="CAD Document Master" heading="ID" propertyName="persistInfo.objectIdentifier">
                    <property name="persistInfo">
                        <property name="objectIdentifier"/>
                    </property>
                </object>
                <column alias="CAD Document Master" heading="Number"
                    propertyName="number" type="java.lang.String">number</column>
                <column alias="CAD Document Master" heading="Name"
                    propertyName="name" type="java.lang.String">name</column>
                <column alias="CAD Document Master" heading="File Name"
                    propertyName="CADName" type="java.lang.String">CADName</column>
            </select>
            <from>
                <table alias="CAD Document Master">WCTYPE|wt.epm.EPMDocumentMaster|com.ricardo.DefaultEPMDocumentMaster</table>
                <table alias="Context">wt.inf.container.WTContainer</table>
            </from>
            <where>
                <compositeCondition type="and">
                    <condition>
                        <operand>
                            <column alias="Context"
                                heading="Context Name"
                                propertyName="name" type="java.lang.String">containerInfo.name</column>
                        </operand>
                        <operator type="like"/>
                        <operand>
                            <parameterTarget name="Context Name"/>
                        </operand>
                    </condition>
                </compositeCondition>
            </where>
            <referenceJoin>
                <join name="containerReference">
                    <aliasTarget alias="CAD Document Master"/>
                    <aliasTarget alias="Context"/>
                </join>
            </referenceJoin>
        </query>
    </statement>
</qml>
